﻿using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core.Utilities;
using HUDHealthcarePortal.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace HUDHealthcareUnitTest.Controller
{
    [TestClass]
    public class ScoreManagerTest
    {
        private DataUploadIntermediateRepository dataUploadRepo;


        [TestInitialize]
        public void Initialize()
        {
            dataUploadRepo = new DataUploadIntermediateRepository();
            AutoMapperInitialize.Initialize();
        }

        [TestMethod]
        [Ignore]
        public void AveragePaymentPeriod_Calculation_Should_As_Formula()
        {
            // Arrange
            //DateTime periodEnd;
            //var result = dataUploadRepo.GetUploadedDataByScoreRange(0, 100).First(p => p.CurrentLiabilities.HasValue
            //    && p.TotalExpenses.HasValue && p.DepreciationExpense.HasValue && p.AmortizationExpense.HasValue);
            //DateTime.TryParse(result.PeriodEnding, out periodEnd);
            //var calculatedResult = result.CurrentLiabilities * DateHelper.DaysFromBeginningOfYearTill(periodEnd) / (result.TotalExpenses - result.DepreciationExpense - result.AmortizationExpense);
            //Assert.AreEqual((double)result.AvgPaymentPeriod.Value, (double)calculatedResult.Value, 0.5, "Tolerance within 0.5");
        }

        [TestMethod]
        [Ignore]
        public void DebtServiceCoverageRatio_Calculation_Should_As_Formula()
        {
            //var result = dataUploadRepo.GetUploadedDataByScoreRange(0, 100).First(p => p.NetIncome.HasValue
            //    && p.FHAInsuredPrincipalPayment.HasValue && p.FHAInsuredInterestPayment.HasValue && p.MortgageInsurancePremium.HasValue);
            //var calculatedResult = result.NetIncome / (result.FHAInsuredPrincipalPayment + result.FHAInsuredInterestPayment + result.MortgageInsurancePremium);
            //Assert.AreEqual((double)result.DebtCoverageRatioScore.Value, (double)calculatedResult.Value, 0.5, "Tolerance within 0.5");
        }
    }
}
