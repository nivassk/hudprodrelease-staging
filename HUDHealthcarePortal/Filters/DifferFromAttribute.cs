﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace HUDHealthCarePortal.Filters
{
    public class DifferFromAttribute : ValidationAttribute
    {
        public DifferFromAttribute(params string[] propertyNames)
        {
            PropertyNames = propertyNames;
        }

        public string[] PropertyNames { get; private set; }
        
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var otherProperties = PropertyNames.Select(validationContext.ObjectType.GetProperty);            
            var otherValues = otherProperties.Select(p => p.GetValue(validationContext.ObjectInstance, null)); //.OfType<string>();

            var thisProperty = validationContext.ObjectType.GetProperty(validationContext.MemberName);
            var thisValue = thisProperty.GetValue(validationContext.ObjectInstance, null);
            foreach(var item in otherValues)
            {
                if (item.Equals(thisValue))
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }
            return null;
        }
    }
}