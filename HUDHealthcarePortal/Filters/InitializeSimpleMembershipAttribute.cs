﻿using Elmah;
using System;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace HUDHealthCarePortal.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                try
                {
                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "HCP_Authentication", "UserID", "UserName", autoCreateTables: true);
                    var roles = (SimpleRoleProvider)Roles.Provider;
                    var membership = (SimpleMembershipProvider)Membership.Provider;

                    if (!roles.RoleExists("Administrator"))
                        roles.CreateRole("Administrator");

                    // internal user roles
                    if (!roles.RoleExists("HUDAdmin"))
                        roles.CreateRole("HUDAdmin");
                    if (!roles.RoleExists("HUDDirector"))
                        roles.CreateRole("HUDDirector");
                    if (!roles.RoleExists("AccountExecutive"))
                        roles.CreateRole("AccountExecutive");
                    if (!roles.RoleExists("WorkflowManager"))
                        roles.CreateRole("WorkflowManager");
                    if (!roles.RoleExists("Attorney"))
                        roles.CreateRole("Attorney");
                    
                    // super user
                    if (!roles.RoleExists("SuperUser"))
                        roles.CreateRole("SuperUser");

                    // external user roles
                    //if (!roles.RoleExists("Lender"))
                    //    roles.CreateRole("Lender");
                    if (!roles.RoleExists("OperatorAccountRepresentative"))
                        roles.CreateRole("OperatorAccountRepresentative");
                    if (!roles.RoleExists("Servicer"))
                        roles.CreateRole("Servicer");
                    if (!roles.RoleExists("LenderAdmin"))
                        roles.CreateRole("LenderAdmin");
                    if (!roles.RoleExists("LenderAccountRepresentative"))
                        roles.CreateRole("LenderAccountRepresentative");
                    if (!roles.RoleExists("BackupAccountManager"))
                        roles.CreateRole("BackupAccountManager");
                    if (!roles.RoleExists("LenderAccountManager"))
                        roles.CreateRole("LenderAccountManager");

                    // seed super user
                    // replace later
                    if (membership.GetUser("sshetty@c3-systems.com", false) == null)
                    {
                        WebSecurity.CreateUserAndAccount("sshetty@c3-systems.com", "Password1$",
                                                            propertyValues: new
                                             {
                                                 FirstName = "Super",
                                                 LastName = "User",    
                                                 PreferredTimeZone = "3",
                                                 ModifiedBy = -1,   // initial machine generated user
                                                 ModifiedOn = DateTime.UtcNow
                                             });
                        Roles.AddUsersToRole(new string[] { "sshetty@c3-systems.com" }, "SuperUser");
                    }

                }
                catch (Exception e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                    throw new InvalidOperationException(
                        string.Format("Error Initializing the Membership: {0}", e.InnerException), e.InnerException);
                    //throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }
        }
    }
}
