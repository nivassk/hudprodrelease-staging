﻿using System.Web.Mvc;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;

namespace HUDHealthcarePortal.Controllers.ManagementReport
{
    public class ManagementReportController : Controller
    {
        IManagementReportManager managementReportMgr;

        public ManagementReportController()
            : this(new ManagementReportManager())
        {

        }

        public ManagementReportController(IManagementReportManager managementReportManager)
        {
            managementReportMgr = managementReportManager;
        }

        //public ActionResult HighRiskProjectReport()
        //{
        //    ReportModel reportModel = managementReportMgr.GetHighRiskProjectReport(HudUserUtil.GetHudUserRole);

        //    return SelectViewBasedOnUserRole(reportModel);
        //}

       

        [HttpGet]
        public ActionResult DoNothing()
        {
            return new EmptyResult();
        }        
    }
}
