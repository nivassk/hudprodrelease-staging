﻿using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using System.Web.Mvc;
using HUDHealthcarePortal.Filters;

namespace HUDHealthcarePortal.Controllers
{
    [Authorize]
    public class MultiLoanController : Controller
    {
        IMultiLoanManager multiLoanMgr;

        public MultiLoanController() : this(new MultiLoanManager())
        {

        }

        public MultiLoanController(IMultiLoanManager multiLoanManager)
        {
            multiLoanMgr = multiLoanManager;
        }

        [HttpGet]
        [AccessDeniedAuthorize(Roles = "Administrator,HUDAdmin,SuperUser")]
        public ActionResult Index()
        {
            return View("MultiLoan");
        }

        [HttpGet]
        public JsonResult GetLoans()
        {
            var multiLoans = multiLoanMgr.GetMultiLoans();
            return Json(multiLoans, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetLoansDetail(int propertyid)
        {
            var multiLoansDetail = multiLoanMgr.GetMultiLoansDetailByPropertyID(propertyid);
            return Json(multiLoansDetail, JsonRequestBehavior.AllowGet);
        }
    }
}
