﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       [dbo].[Lender_DataUpload_Intermediate].[NonClusteredIndex-20170105-120559_FHANumber] (Index)
       [dbo].[Lender_DataUpload_Intermediate].[NonClusteredIndex-20170105-120611_LenderID] (Index)
       [dbo].[Lender_DataUpload_Intermediate].[NonClusteredIndex-20170105-120622_PeriodEnding] (Index)
       [dbo].[Lender_DataUpload_Intermediate].[NonClusteredIndex-20170105-120635_MonthsInPeriod] (Index)
     Alter
       [dbo].[fn_HCP_GeLatestUpload] (Function)
       [dbo].[usp_HCP_CalculateErrors] (Procedure)
       [dbo].[usp_HCP_DistributeQuaterly] (Procedure)
       [dbo].[vwComment].[MS_DiagramPane1] (Extended Property)

** Supporting actions

The following SqlCmd variables are not defined in the target scripts: IntermDB.

