﻿CREATE FUNCTION [dbo].[fn_HCP_GetPeriodEndingDate]
(
  @FHANumber NVARCHAR(100),
  @MIP INT,
  @Year INT
)
RETURNS DATETIME
AS
BEGIN
  DECLARE @MaxPEDt DATETIME
  DECLARE @MaxMIP INT
  DECLARE @YearCount INT
  DECLARE @ResultPEDt DATETIME
  SET @ResultPEDt = (SELECT MAX(CONVERT(DATETIME, PeriodEnding)) 
            FROM [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate 
            WHERE FHANumber = @FHANumber AND 
            CONVERT(INT, MonthsInPeriod) = @MIP AND YEAR(CONVERT(DATETIME, PeriodEnding)) = @Year)
  IF @ResultPEDt IS NOT NULL
  BEGIN
    RETURN EOMONTH(@ResultPEDt)
  END
  ELSE
  BEGIN
    SET @MaxMIP = (SELECT MAX(CONVERT(INT, MonthsInPeriod))
            FROM [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate AS Lender_DataUpload_Intermediate_1
            WHERE (FHANumber = @FHANumber))
    SET @MaxPEDt = (SELECT MAX(CONVERT(DATETIME, PeriodEnding))
            FROM [$(DatabaseName)].dbo.Lender_DataUpload_Intermediate
            WHERE (FHANumber = @FHANumber) AND (CONVERT(INT, MonthsInPeriod) = @MaxMIP)
             )
    IF ISNULL(@MaxMIP,0) = 0
    --RETURN NULL
      If @MIP = 3 return Convert(datetime, '3/31/' + Cast(@Year as nvarchar(4)))
      else if @MIP = 6 return Convert(datetime, '6/30/' + Cast(@Year as nvarchar(4)))
      else if @MIP = 9 return Convert(datetime, '9/30/' + Cast(@Year as nvarchar(4)))
      else if @MIP = 12 return Convert(datetime, '12/31/' + Cast(@Year as nvarchar(4)))
      else return EOMONTH(Convert(datetime, CONVERT(varchar(10), @MIP) + '/1/' + Cast(@Year as nvarchar(4))))
    --else return null
    else If @MaxMIP > 12 
      set @MaxMIP = 12

    -- MIP is same as Max MIP but year is different
    IF @MaxMIP = @MIP
    BEGIN
      IF YEAR(@MaxPEDt) <> @Year
      BEGIN
        SET @YearCount = CONVERT(INT,(@Year - YEAR(@MaxPEDt)))
        SET @ResultPEDt = DATEADD(YY, @YearCount,@MaxPEDt)
      END
    END
    ELSE 
    BEGIN
      IF @MaxMIP = 12
      BEGIN
        IF (@MaxMIP - 3) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, -3, @MaxPEDt)
        END
        ELSE IF(@MaxMIP - 6) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, -6, @MaxPEDt)
        END
        ELSE IF(@MaxMIP - 9) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, -9, @MaxPEDt)
        END
      END
      ELSE IF @MaxMIP = 9
      BEGIN
        IF (@MaxMIP + 3) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, 3, @MaxPEDt)
        END
        ELSE IF(@MaxMIP - 3) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, -3, @MaxPEDt)
        END
        ELSE IF(@MaxMIP - 6) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, -6, @MaxPEDt)
        END
      END
      ELSE IF @MaxMIP = 6
      BEGIN
        IF (@MaxMIP + 6) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, 6, @MaxPEDt)
        END
        ELSE IF(@MaxMIP + 3) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, 3, @MaxPEDt)
        END
        ELSE IF(@MaxMIP - 3) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, -3, @MaxPEDt)
        END
      END
      ELSE IF @MaxMIP = 3
      BEGIN
        IF (@MaxMIP + 9) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, 9, @MaxPEDt)
        END
        ELSE IF(@MaxMIP + 6) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, 6, @MaxPEDt)
        END
        ELSE IF(@MaxMIP + 3) = @MIP
        BEGIN
          SET @ResultPEDt =  DATEADD(month, 3, @MaxPEDt)
        END
      END
      Else
      Begin
        return EOMONTH(Convert(datetime, CONVERT(varchar(10), @MaxMIP) + '/1/' + Cast(@Year as nvarchar(4))))
      End
      IF YEAR(@MaxPEDt) <> @Year
      BEGIN
        SET @YearCount = CONVERT(INT,(@Year - YEAR(@MaxPEDt)))
        SET @ResultPEDt =  DATEADD(YY, @YearCount,@ResultPEDt)
      END
    END
  END
  RETURN EOMONTH(@ResultPEDt)
END




