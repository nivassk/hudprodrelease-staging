﻿CREATE FUNCTION [dbo].[fn_HCP_GetQuarterToken]
(
	@PeriodEnding  DATETIME
   ,@MonthInPeriod INT
)
RETURNS VARCHAR(6)
AS
BEGIN
	DECLARE @YearPart VARCHAR(4)
	DECLARE @QuarterPart VARCHAR(2)
	SET @YearPart = CAST(YEAR(@PeriodEnding) AS VARCHAR(4))
	SET @QuarterPart =
      CASE @MonthInPeriod
         WHEN 3 THEN 'Q1'
         WHEN 6 THEN 'Q2'
         WHEN 9 THEN 'Q3'
         WHEN 12 THEN 'Q4'
         ELSE '??'
      END   

    RETURN @YearPart + @QuarterPart
END

