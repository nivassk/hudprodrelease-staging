﻿CREATE TABLE [dbo].[Prod_LoanCommittee]
(

	LoanCommitteeId uniqueidentifier not Null,
	SequenceId int not Null,
	FHANumber nvarchar(20) not Null,
	ApplicationTaskinstanceId uniqueidentifier not Null,
	LenderId int not Null,
	ProjectScheduledForLC int Null,
	LoanCommitteDate datetime Null,
	LCRecommendation INT Null,
	A7Presentation int Null,
	TwoStageSubmittal int Null,
	LCComments nvarchar(500) Null,
	ProjectName nvarchar(500) Null,
	ProjectType int Null,
	NonProfit nvarchar(20) Null,
	LoanAmount decimal(19,2) Null,
	Underwriter nvarchar(500) Null,
	Wlm nvarchar(500) Null,
	Ogc nvarchar(500) Null,
	Appraiser nvarchar(500) Null,
	Lender nvarchar(500) Null,
	CreatedBy int not Null,
	CreatedOn datetime not Null,
	ModifiedBy int not Null,
	ModifiedOn datetime not Null,
	Deleted_Ind bit not Null,

 [LCDecision] INT NULL, 
    [DecisionDate] DATETIME NULL, 
    [ChangeToLoanCommitteeDate] NCHAR(30) NULL, 
    CONSTRAINT [PK_LoanCommitteeId] PRIMARY KEY CLUSTERED 
(
	[LoanCommitteeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
