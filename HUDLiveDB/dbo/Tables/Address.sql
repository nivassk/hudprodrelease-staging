﻿
CREATE TABLE [dbo].[Address](
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[AddressLine1] [nvarchar](80) NULL,
	[AddressLine2] [nvarchar](80) NULL,
	[City] [nvarchar](56) NULL,
	[StateCode] [nvarchar](2) NULL,
	[ZIP] [nvarchar](20) NULL,
	[ZIP4_Code] [nvarchar](10) NULL,
	[PhonePrimary] [nvarchar](25) NULL,
	[PhoneAlternate] [nvarchar](25) NULL,
	[Fax] [nvarchar](25) NULL,
	[Email] [nvarchar](100) NULL,
	[Title] [nvarchar](128) NULL,
	[Organization] [nvarchar](128) NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
	[Deleted_Ind] [bit] NULL,
 CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Address]  WITH CHECK ADD  CONSTRAINT [FK_Address_State1] FOREIGN KEY([StateCode])
REFERENCES [dbo].[State] ([StateCode])
GO

ALTER TABLE [dbo].[Address] CHECK CONSTRAINT [FK_Address_State1]
GO

