﻿
CREATE TABLE [dbo].[Prod_SharepointScreen](
	[ScreenId] [int] IDENTITY(1,1) NOT NULL,
	[IsLongTermHold] [bit] NULL,
	[DateOfHold] [datetime] NULL,
	[ReasonForHold] [varchar](500) NULL,
	[IsLoanCommitteApproved] [bit] NULL,
	[LoanCommitteApprovedDate] [datetime] NULL,
	[IsFirmCommitmentIssued] [bit] NULL,
	[FirmCommitmentIssuedDate] [datetime] NULL,
	[IsEarlyCommencementRequested] [bit] NULL,
	[EarlyCommencementRequestedDate] [datetime] NULL,
	[EarlyCommencementIssuedDate] [datetime] NULL,
	[IsLDLIssued] [bit] NULL,
	[LDLIssuedDate] [datetime] NULL,
	[LDLCompleteResponseReceived] [datetime] NULL,
	[MiscellaneousComments] [varchar](500) NULL,
	[OtherQueueWorkComments] [varchar](500) NULL,
	[SelectedAEId] [int] NULL,
	[IsIremsCompleted] [bit] NULL,
	[IsAppsCompleted] [bit] NULL,
	[IsIremsPrintoutSaved] [bit] NULL,
	[IsMirandaCompletedCMSListCheck] [bit] NULL,
	[ContractorContractPrice] [decimal](18, 2) NULL,
	[ContractorSecondaryAmtPaid] [decimal](18, 2) NULL,
	[ContractorAmountPaid] [decimal](18, 2) NULL,
	[OGCComment] [nvarchar](500) NULL,
	[EnvironmentalComment] [varchar](500) NULL,
	[SurveyComment] [varchar](500) NULL,
	[ContractorComment] [varchar](1000) NULL,
	[ClosingComment] [varchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedON] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[TaskinstanceId] [uniqueidentifier] NOT NULL,
	[FHANumber] [varchar](15) NOT NULL,
	[IsCloserContractor] [bit] NULL,
	[ContractorContactName] [varchar](50) NULL,
	[ClosingProgramSpecialistId] [int] NULL,
	[IsCostCertRecieved] [bit] NULL,
	[CostCertRecievedDate] [datetime] NULL,
	[IsCostCertCompletedAndIssued] [bit] NULL,
	[CostCertIssueDate] [datetime] NULL,
	[Is290Completed] [bit] NULL,
	[Two90CompletedDate] [datetime] NULL,
	[IsActiveNCRE] [bit] NULL,
	[NCREDueDate] [datetime] NULL,
	[NCREInitalBalance] [decimal](18, 0) NULL,
	[ClosingPackageRecievedDate] [datetime] NULL,
	[ClosingAEId] [int] NULL,
	[ClosingCloserId] [int] NULL,
	[InitialClosingDate] [datetime] NULL,
	[BackupOgcAddressId] [int] NULL,
	[OgcAddressId] [int] NULL,
	[AppraisalComment] [varchar](500) NULL,
	[ClosingDateCreatedOn] [datetime] NULL,
	[ClosingDateModifiedOn] [datetime] NULL,
 CONSTRAINT [PK__Prod_Sha__0AB60FA5E1852951] PRIMARY KEY CLUSTERED 
(
	[ScreenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_DateOfHold]  DEFAULT (NULL) FOR [DateOfHold]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_LoanCommitteApprovedDate]  DEFAULT (NULL) FOR [LoanCommitteApprovedDate]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_FirmCommitmentIssuedDate]  DEFAULT (NULL) FOR [FirmCommitmentIssuedDate]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_EarlyCommencementRequestedDate]  DEFAULT (NULL) FOR [EarlyCommencementRequestedDate]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_EarlyCommencementIssuedDate]  DEFAULT (NULL) FOR [EarlyCommencementIssuedDate]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_LDLIssuedDate]  DEFAULT (NULL) FOR [LDLIssuedDate]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_LDLCompleteResponseReceived]  DEFAULT (NULL) FOR [LDLCompleteResponseReceived]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_ContractorContractPrice]  DEFAULT ((0.0)) FOR [ContractorContractPrice]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_ContractorSecondaryAmtPaid]  DEFAULT ((0.0)) FOR [ContractorSecondaryAmtPaid]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_ContractorAmountPaid]  DEFAULT ((0.0)) FOR [ContractorAmountPaid]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_CreatedON]  DEFAULT (NULL) FOR [CreatedON]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF_Prod_SharepointScreen_ModifiedOn]  DEFAULT (NULL) FOR [ModifiedOn]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF__Prod_Shar__IsClo__7DB89C09]  DEFAULT ((0)) FOR [IsCloserContractor]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF__Prod_Shar__IsCos__7EACC042]  DEFAULT ((0)) FOR [IsCostCertRecieved]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF__Prod_Shar__IsCos__7FA0E47B]  DEFAULT ((0)) FOR [IsCostCertCompletedAndIssued]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF__Prod_Shar__Is290__009508B4]  DEFAULT ((0)) FOR [Is290Completed]
GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ADD  CONSTRAINT [DF__Prod_Shar__IsAct__01892CED]  DEFAULT ((0)) FOR [IsActiveNCRE]
GO

CREATE Trigger [dbo].[Trigger_Audit_Sharepoint] ON [dbo].[Prod_SharepointScreen]
AFTER UPDATE, INSERT
AS
BEGIN
SET NOCOUNT ON;
INSERT INTO SharepointPortalAudit(TaskInstanceId, UserId, CreatedDate)
SELECT TaskInstanceId, ModifiedBy, ModifiedOn  FROM inserted
END

GO

ALTER TABLE [dbo].[Prod_SharepointScreen] ENABLE TRIGGER [Trigger_Audit_Sharepoint]
GO
