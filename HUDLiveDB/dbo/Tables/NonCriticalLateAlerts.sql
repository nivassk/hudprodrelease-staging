﻿
CREATE TABLE [dbo].[NonCriticalLateAlerts](
	[NonCriticalLateAlertId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [int] NOT NULL,
	[LenderId] [int] NOT NULL,
	[FhaNumber] [nvarchar](15) NOT NULL,
	[TimeSpanId] [int] NULL,
	[HCP_EmailId] [int] NULL,
	[RecipientIds] [nvarchar](256) NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_NonCriticalLateAlerts] PRIMARY KEY CLUSTERED 
(
	[NonCriticalLateAlertId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[NonCriticalLateAlerts]  WITH CHECK ADD  CONSTRAINT [fk_NonCriticalLateAlerts_HCP_Email_EmailId] FOREIGN KEY([HCP_EmailId])
REFERENCES [dbo].[HCP_Email] ([EmailId])
GO

ALTER TABLE [dbo].[NonCriticalLateAlerts] CHECK CONSTRAINT [fk_NonCriticalLateAlerts_HCP_Email_EmailId]
GO

