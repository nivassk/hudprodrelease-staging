﻿
CREATE TABLE [dbo].[HCP_FunctionPoint_Ref](
	[HCP_FunctionPoint_ID] [int] NOT NULL,
	[HCP_FunctionPoint_Name] [nchar](50) NOT NULL,
	[HCP_FunctionPoint_Desc] [varchar](256) NOT NULL,
	[Deleted_Ind] [bit] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
 CONSTRAINT [PK_HCP_FunctionPoint] PRIMARY KEY CLUSTERED 
(
	[HCP_FunctionPoint_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

