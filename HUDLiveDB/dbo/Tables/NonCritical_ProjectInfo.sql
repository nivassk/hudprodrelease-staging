﻿
CREATE TABLE [dbo].[NonCritical_ProjectInfo](
	[PropertyId] [int] NOT NULL,
	[LenderId] [int] NOT NULL,
	[FhaNumber] [nvarchar](15) NOT NULL,
	[ClosingDate] [datetime] NULL,
	[EndingDate] [datetime] NULL,
	[InitialNCREBalance] [decimal](19, 2) NULL,
	[CurrentBalance] [decimal](19, 2) NULL,
	[IsAmountConfirmed] [bit] NULL,
	[AmountConfirmationStatus] [nvarchar](10) NULL,
	[ExtensionApprovalDate] [datetime] NULL,
	[SuggestedAmount] [decimal](19, 2) NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_NonCritical_ProjectInfo] PRIMARY KEY CLUSTERED 
(
	[PropertyId] ASC,
	[LenderId] ASC,
	[FhaNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

