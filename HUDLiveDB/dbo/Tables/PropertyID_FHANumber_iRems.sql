﻿
CREATE TABLE [dbo].[PropertyID_FHANumber_iREMS](
	[PropertyID] [int] NOT NULL,
	[PropertyName] [nvarchar](100) NOT NULL,
	[FHANumber] [nvarchar](15) NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NOT NULL,
	[OnBehalfOfBy] [int] NULL,
 CONSTRAINT [PK_PropertyID_FHANumber_iREMS] PRIMARY KEY CLUSTERED 
(
	[PropertyID] ASC,
	[FHANumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PropertyID_FHANumber_iREMS]  WITH CHECK ADD  CONSTRAINT [FK_PropertyID_FHANumber_iREMS_FHAInfo_iREMS] FOREIGN KEY([FHANumber])
REFERENCES [dbo].[FHAInfo_iREMS] ([FHANumber])
GO

ALTER TABLE [dbo].[PropertyID_FHANumber_iREMS] CHECK CONSTRAINT [FK_PropertyID_FHANumber_iREMS_FHAInfo_iREMS]
GO

ALTER TABLE [dbo].[PropertyID_FHANumber_iREMS]  WITH CHECK ADD  CONSTRAINT [FK_PropertyID_FHANumber_iREMS_PropertyInfo_iREMS] FOREIGN KEY([PropertyID])
REFERENCES [dbo].[PropertyInfo_iREMS] ([PropertyID])
GO

ALTER TABLE [dbo].[PropertyID_FHANumber_iREMS] CHECK CONSTRAINT [FK_PropertyID_FHANumber_iREMS_PropertyInfo_iREMS]
GO

