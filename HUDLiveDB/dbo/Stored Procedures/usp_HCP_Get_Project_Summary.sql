﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_Project_Summary] 
(
	@ReportLevel nvarchar(100),
	@UserName nvarchar(100),
    @UserType nvarchar(100),
    @ReportType nvarchar(100),
    @WlmId nvarchar(100),
    @AeId  nvarchar(100),
    @LenderId  nvarchar(100),
    @MinUploadDate DATETIME,
    @MaxUploadDate DATETIME,
	@IsHighLoan BIT,
	@IsDebtCoverageRatio BIT,
	@IsWorkingCapital BIT,
	@IsDaysCashOnHand BIT,
	@IsDaysInAcctReceivable BIT,
	@IsAvgPaymentPeriod BIT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    DECLARE @ReportHeader nvarchar(10)
    DECLARE @SuLevel nvarchar(14)
    DECLARE @WlmLevel nvarchar(16)
    DECLARE @AeLevel nvarchar(17)
    SET @SuLevel = 'SuperUserLevel'
    SET @WlmLevel = 'WLMUserLevel'
    SET @AeLevel = 'AEUserLevel'
	DECLARE @SuType nvarchar(14)
    DECLARE @WlmType nvarchar(16)
    DECLARE @AeType nvarchar(17)
    SET @SuType = 'SuperUser'
    SET @WlmType = 'WLMUser'
    SET @AeType = 'AEUser'
	DECLARE @RatioExceptionsReport nvarchar(21)
	DECLARE @allRatioExceptionsReport BIT
	SET @RatioExceptionsReport = 'RatioExceptionsReport'

    IF @MinUploadDate IS NULL
        SET @MinUploadDate = '01/01/1753'
    IF @MaxUploadDate IS NULL
        SET @MaxUploadDate = '12/31/9999'
		
    IF @UserType = @AeType AND @AeId IS NULL
        SET @AeId = (SELECT TOP 1 pm.HUD_Project_Manager_ID
                     FROM   dbo.Address addr,
                            dbo.HUD_Project_Manager pm
                     WHERE  addr.Email = @UserName
    	             AND    addr.AddressID = pm.AddressID)
    ELSE IF @UserType = @WlmType AND @WlmId IS NULL
        SET @WlmId = (SELECT TOP 1 wlm.HUD_WorkLoad_Manager_ID
                      FROM   dbo.Address addr,
                             dbo.HUD_WorkLoad_Manager wlm
                      WHERE  addr.Email = @UserName
    	              AND    addr.AddressID = wlm.AddressID)

    	DECLARE @GroupOwnerList TABLE (
    	    GroupOwnerId INT,
    	    GroupOwnerName NVARCHAR(100)
    	)

    IF @ReportLevel = @SuLevel
    BEGIN
        SET @ReportHeader = 'WLM'
    	INSERT INTO @GroupOwnerList 
    	    SELECT HUD_WorkLoad_Manager_ID GroupOwnerId,
    	           HUD_WorkLoad_Manager_Name GroupOwnerName 
    		FROM [dbo].[HUD_WorkLoad_Manager]
    END
    ELSE IF @ReportLevel = @WlmLevel
    BEGIN
        SET @ReportHeader = 'AE'
    	INSERT INTO @GroupOwnerList 
    	    SELECT HUD_Project_Manager_ID GroupOwnerId,
    	           HUD_Project_Manager_Name GroupOwnerName 
    		FROM [dbo].[HUD_Project_Manager]
    END
    ELSE
    BEGIN
        SET @ReportHeader = 'LENDER'
    	INSERT INTO @GroupOwnerList 
    	    SELECT LenderID GroupOwnerId,
    	           Lender_Name GroupOwnerName 
    		FROM [dbo].[LenderInfo]
    END

    IF     @ReportType = @RatioExceptionsReport 
	   AND @IsDebtCoverageRatio = 0
	   AND @IsWorkingCapital = 0
	   AND @IsDaysCashOnHand = 0
	   AND @IsDaysInAcctReceivable = 0
	   AND @IsAvgPaymentPeriod = 0
		SET @allRatioExceptionsReport = 1
	ELSE
		SET @allRatioExceptionsReport = 0

    CREATE TABLE #ProjectSummary
    (
        ProjectSummaryID int IDENTITY(1,1) NOT NULL,
    	WorkloadManagerId INT,
        WorkloadManager NVARCHAR(100),
        AccountExecutiveId INT,
        AccountExecutive NVARCHAR(100),
        LenderId INT,
        LenderName NVARCHAR(100),
    	ReportLevel NVARCHAR(30),
    	GroupOwnerId INT,
    	GroupOwnerName NVARCHAR(100),
    	NumberOfProjects INT,
    	TotalUnpaidPrincipalBalance DECIMAL,
    	DebtCoverageRatio DECIMAL,
    	WorkingCapital DECIMAL,
        DaysCashOnHand DECIMAL,
        DaysInAcctReceivable DECIMAL,
        AvgPaymentPeriod DECIMAL,
        UnpaidPrincipalBalance DECIMAL
    )


    INSERT INTO #ProjectSummary
    SELECT NULL									"WorkloadManagerId"
	      ,NULL									"WorkloadManager"
		  ,NULL									"AccountExecutiveId"
		  ,NULL									"AccountExecutive"
		  ,NULL									"LenderId"
		  ,NULL									"LenderName"
		  ,@ReportHeader						"ReportLevel"
    	  ,ISNULL(gol.GroupOwnerId,-1)			"GroupOwnerId"
    	  ,ISNULL(gol.GroupOwnerName,'Not Assigned')			"GroupOwnerName"
          ,ISNULL(count(*), 0) 							"NumberOfProjects"
          ,ISNULL(sum(Amortized_Unpaid_Principal_Bal), 0)	"TotalUnpaidPrincipalBalance"
		  ,ISNULL(count(case when DebtCoverageRatio < 1.2 then 1 else null end), 0)	"DebtCoverageRatio"
		  ,ISNULL(count(case when WorkingCapital < 1.2 then 1 else null end), 0)	"WorkingCapital"
		  ,ISNULL(count(case when DaysCashOnHand < 14 then 1 else null end), 0)	"DaysCashOnHand"
		  ,ISNULL(count(case when DaysInAcctReceivable > 70 then 1 else null end), 0)	"DaysInAcctReceivable"
		  ,ISNULL(count(case when AvgPaymentPeriod > 70 then 1 else null end), 0)	"AvgPaymentPeriod"
		  ,ISNULL(count(case when Amortized_Unpaid_Principal_Bal > 25000000 then 1 else null end), 0)	"UnpaidPrincipalBalance"
	
      FROM (SELECT top (2147483647) * FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate]
            WHERE ldi_id in (SELECT top (2147483647) MAX(ldi.ldi_id) ldi_id	
                             FROM [$(IntermDB)].[dbo].[Lender_DataUpload_Intermediate] ldi,
    						      [dbo].fn_HCP_GetPropertyList(@WlmId, @AeId, @LenderId) fp
                             WHERE ldi.PropertyID =fp.PropertyID
                             GROUP BY ldi.PropertyID)
			AND	  CONVERT(DATE, DataInserted) >= @MinUploadDate
			AND   CONVERT(DATE, DataInserted) <= @MaxUploadDate
			AND   ((@allRatioExceptionsReport = 0
    	   			AND   (@IsDebtCoverageRatio = 0 OR DebtCoverageRatio < 1.2)
					AND   (@IsDaysCashOnHand = 0 OR DaysCashOnHand < 14)
					AND   (@IsAvgPaymentPeriod = 0 OR AvgPaymentPeriod > 70)
					AND   (@IsWorkingCapital = 0 OR WorkingCapital < 1.2)
					AND   (@IsDaysInAcctReceivable = 0 OR DaysInAcctReceivable > 70)
				   ) OR 
				   (@allRatioExceptionsReport = 1 AND
				    (     DebtCoverageRatio < 1.2
					 OR   DaysCashOnHand < 14
					 OR   AvgPaymentPeriod > 70
					 OR   WorkingCapital < 1.2
					 OR   DaysInAcctReceivable > 70
				    )
				   )
				  )
		   ) ld
    	   inner join [dbo].[ProjectInfo] pri
    	   on pri.PropertyID = ld.PropertyID
    	   and   pri.FHANumber = ld.FHANumber
		   AND (@IsHighLoan = 0 OR pri.Amortized_Unpaid_Principal_Bal >= 25000000)
    	   left outer join @GroupOwnerList gol
    	   on    (@ReportLevel = @SuLevel AND gol.GroupOwnerId = pri.HUD_WorkLoad_Manager_ID)
    	      OR (@ReportLevel = @WlmLevel AND gol.GroupOwnerId = pri.HUD_Project_Manager_ID)
    	      OR (@ReportLevel = @AeLevel AND gol.GroupOwnerId = pri.LenderID)
      	   group by GroupOwnerId, GroupOwnerName
    	   ORDER BY GroupOwnerName

    IF @ReportLevel = @SuLevel
        UPDATE #ProjectSummary
		SET WorkloadManagerId = GroupOwnerId,
		    WorkloadManager = GroupOwnerName
    ELSE IF @ReportLevel = @WlmLevel
        UPDATE #ProjectSummary
		SET WorkloadManagerId = @WlmId,
			AccountExecutiveId = GroupOwnerId,
			AccountExecutive = GroupOwnerName
    ELSE
        UPDATE #ProjectSummary
		SET WorkloadManagerId = @WlmId,
			AccountExecutiveId = @AeId,
			LenderId = GroupOwnerId,
			LenderName = GroupOwnerName

    INSERT INTO #ProjectSummary
    SELECT CASE
	       WHEN @ReportLevel = @SuLevel
		   THEN
		       0
		   ELSE
		       @WlmId
		   END									"WorkloadManagerId"
    	  ,'Total Projects'								"WorkloadManager"
          ,CASE
	       WHEN @ReportLevel = @WlmLevel
		   THEN
		       0
		   ELSE
		       @AeId
		   END									"AccountExecutiveId"
    	  ,'Total Projects'								"AccountExecutive"
    	  ,CASE
	       WHEN @ReportLevel = @AeLevel
		   THEN
		       0
		   ELSE
		       NULL
		   END									"LenderId"
    	  ,'Total Projects'								"LenderName"
    	  ,@ReportHeader						"ReportLevel"
    	  ,0									"GroupOwnerId"
    	  ,'Total Projects'								"GroupOwnerName"
          ,ISNULL(sum(NumberOfProjects), 0) 			"NumberOfProjects"
          ,ISNULL(sum(TotalUnpaidPrincipalBalance), 0)	"TotalUnpaidPrincipalBalance"
          ,ISNULL(sum(DebtCoverageRatio), 0) 			"DebtCoverageRatio"
          ,ISNULL(sum(WorkingCapital), 0) 				"WorkingCapital"
          ,ISNULL(sum(DaysCashOnHand), 0) 				"DaysCashOnHand"
          ,ISNULL(sum(DaysInAcctReceivable), 0)			"DaysInAcctReceivable"
          ,ISNULL(sum(AvgPaymentPeriod), 0) 			"AvgPaymentPeriod"
          ,ISNULL(sum(UnpaidPrincipalBalance), 0) 		"UnpaidPrincipalBalance"
    	  from #ProjectSummary

    SELECT * from #ProjectSummary
    ORDER BY ProjectSummaryID

END


