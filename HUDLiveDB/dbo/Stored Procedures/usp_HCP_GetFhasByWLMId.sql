﻿--execute [usp_HCP_GetFhasByWLMId] '18'

create PROCEDURE [dbo].[usp_HCP_GetFhasByWLMId]
(
@wlmId varchar(MAX)
)
AS


select distinct 
	pinfo.FHANumber,
	pinfo.ProjectName as FHADescription,
	pinfo.HUD_WorkLoad_Manager_ID,
	null
from dbo.ProjectInfo pinfo
join Lender_FHANumber lfha on lfha.FHANumber = pinfo.FHANumber
where lfha.FHA_EndDate is null 
and pinfo.HUD_WorkLoad_Manager_ID = @wlmId
order by pinfo.FHANumber,pinfo.ProjectName

