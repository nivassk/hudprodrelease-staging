﻿CREATE PROCEDURE [dbo].[usp_HCP_GetOpenChildTasksByParentTask]
(
@AEEmail  varchar(max)
)
AS
select * from [$(TaskDB)].[dbo].[Task] t
inner join  [$(TaskDB)].[dbo].[ParentChildTask] pc on t.TaskInstanceId = pc.ChildTaskInstanceId
inner join (select lt.TaskInstanceId, max(lt.StartTime) as MaxDate 
                      from [$(TaskDB)].[dbo].[Task] lt  group by lt.TaskInstanceId ) l on l.TaskInstanceId = t.TaskInstanceId and t.StartTime = l.MaxDate
where t.AssignedTo=@AEEmail and t. taskstepid =16 or t. taskstepid =14  or t. taskstepid =15


GO

