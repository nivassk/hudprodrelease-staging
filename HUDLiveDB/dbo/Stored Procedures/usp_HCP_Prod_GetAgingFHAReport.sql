﻿
-- =============================================
-- Author:		<Rashmi Ganapathy>
-- Create date: <05/19/2017>
-- Description:	<Get Aging FHA Report>
-- =============================================
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetAgingFHAReport]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select FHANumber, ProjectTypeName, Lender_Name, (au.FirstName +' '+ au.LastName) as LenderUserName, 
	'180' as NotificationType, Convert(bit,1) as IsNotificationSent, fr.ModifiedOn as FhaRequestCompletionDate
	from Prod_FHANumberRequest fr
	join Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId 
	join LenderInfo li on fr.LenderId = li.LenderID
	join HCP_Authentication au on fr.CreatedBy = au.UserID
	where One80Warning = 1 

	UNION

	select FHANumber, ProjectTypeName, Lender_Name, (au.FirstName +' '+ au.LastName) as LenderUserName, 
	'270' as NotificationType, Convert(bit,1) as IsNotificationSent, fr.ModifiedOn as FhaRequestCompletionDate
	from Prod_FHANumberRequest fr
	join Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId 
	join LenderInfo li on fr.LenderId = li.LenderID
	join HCP_Authentication au on fr.CreatedBy = au.UserID
	where Two70Warning = 1 

	UNION

	select fr.FHANumber, ProjectTypeName, Lender_Name, (au.FirstName +' '+ au.LastName) as LenderUserName, 
	'180' as NotificationType, Convert(bit,0) as IsNotificationSent, fr.ModifiedOn as FhaRequestCompletionDate
	from Prod_FHANumberRequest fr
	join Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId 
	join LenderInfo li on fr.LenderId = li.LenderID
	join HCP_Authentication au on fr.CreatedBy = au.UserID
	join Prod_NextStage ns on fr.FHANumber = ns.FhaNumber
	where IsReadyForApplication = 1 and CompletedPgTypeId = 4 
	and (fr.ProjectTypeId in (1,2,3,4,8) OR fr.ConstructionStageTypeId = 1)
	and NextPgTaskInstanceId is null and One80Warning is null
	and DATEDIFF(dd,fr.ModifiedOn,getdate()) >= 150

	UNION

	select fr.FHANumber, ProjectTypeName, Lender_Name, (au.FirstName +' '+ au.LastName) as LenderUserName, 
	'180' as NotificationType, Convert(bit,0) as IsNotificationSent, fr.ModifiedOn as FhaRequestCompletionDate
	from Prod_FHANumberRequest fr
	join Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId 
	join LenderInfo li on fr.LenderId = li.LenderID
	join HCP_Authentication au on fr.CreatedBy = au.UserID
	join Prod_NextStage ns on fr.FHANumber = ns.FhaNumber
	where IsReadyForApplication = 1 and CompletedPgTypeId = 4 
	and fr.ConstructionStageTypeId = 2
	and NextPgTaskInstanceId is null and One80Warning is null
	and DATEDIFF(dd,fr.ModifiedOn,getdate()) >= 150

	UNION

	select fr.FHANumber, ProjectTypeName, Lender_Name, (au.FirstName +' '+ au.LastName) as LenderUserName, 
	'270' as NotificationType, Convert(bit,0) as IsNotificationSent, fr.ModifiedOn as FhaRequestCompletionDate
	from Prod_FHANumberRequest fr
	join Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId 
	join LenderInfo li on fr.LenderId = li.LenderID
	join HCP_Authentication au on fr.CreatedBy = au.UserID
	join Prod_NextStage ns on fr.FHANumber = ns.FhaNumber
	where IsReadyForApplication = 1 and CompletedPgTypeId = 4 
	and (fr.ProjectTypeId in (1,2,3,4,8) OR fr.ConstructionStageTypeId = 1)
	and NextPgTaskInstanceId is null and One80Warning is not null
	and Two70Warning is null
	and DATEDIFF(dd,fr.ModifiedOn,getdate()) >= 240

	UNION

	select fr.FHANumber, ProjectTypeName, Lender_Name, (au.FirstName +' '+ au.LastName) as LenderUserName, 
	'270' as NotificationType, Convert(bit,0) as IsNotificationSent, fr.ModifiedOn as FhaRequestCompletionDate
	from Prod_FHANumberRequest fr
	join Prod_ProjectType pt on fr.ProjectTypeId = pt.ProjectTypeId 
	join LenderInfo li on fr.LenderId = li.LenderID
	join HCP_Authentication au on fr.CreatedBy = au.UserID
	join Prod_NextStage ns on fr.FHANumber = ns.FhaNumber
	where IsReadyForApplication = 1 and CompletedPgTypeId = 4 
	and fr.ConstructionStageTypeId = 2
	and NextPgTaskInstanceId is null and One80Warning is not null
	and Two70Warning is null
	and DATEDIFF(dd,fr.ModifiedOn,getdate()) >= 240


END

GO

