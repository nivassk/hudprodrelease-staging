﻿
CREATE PROCEDURE [dbo].[usp_HCP_Get_Portfolio_Aggregate]
(
@QuarterToken VARCHAR(6)
)
AS
  SELECT 
	COUNT(1) AS Portfolio_Count, 
	bb.Portfolio_Name, 
	bb.Portfolio_Number,
	SUM(ISNULL(g.MonthsInPeriod, 0))/IIF(COUNT(g.MonthsInPeriod)=0, 1.0, COUNT(g.MonthsInPeriod)) AS MonthsInPeriodAvg,
	SUM(ISNULL(g.UnitsInFacility, 0)) AS UnitsInFacility,
	SUM(ISNULL(g.OperatingCash, 0)) AS OperatingCash,
	SUM(ISNULL(g.Investments, 0)) AS Investments,
	SUM(ISNULL(g.ReserveForReplacementEscrowBalance, 0)) AS ReserveForReplacementEscrowBalance,
	SUM(ISNULL(g.AccountsReceivable, 0)) AS AccountsReceivable,
	SUM(ISNULL(g.CurrentAssets, 0)) AS CurrentAssets,
	SUM(ISNULL(g.CurrentLiabilities, 0)) AS CurrentLiabilities,
	SUM(ISNULL(g.TotalRevenues, 0)) AS TotalRevenues,
	SUM(ISNULL(g.RentLeaseExpense, 0)) AS RentLeaseExpense,
	SUM(ISNULL(g.DepreciationExpense, 0)) AS DepreciationExpense,
	SUM(ISNULL(g.AmortizationExpense, 0)) AS AmortizationExpense,
	SUM(ISNULL(g.TotalExpenses, 0)) AS TotalExpenses,
	SUM(ISNULL(g.NetIncome, 0)) AS NetIncome,
	SUM(ISNULL(g.ReserveForReplacementDeposit, 0)) AS ReserveForReplacementDeposit,
	SUM(ISNULL(g.FHAInsuredPrincipalPayment, 0)) AS FHAInsuredPrincipalPayment,
	SUM(ISNULL(g.FHAInsuredInterestPayment, 0)) AS FHAInsuredInterestPayment,
	SUM(ISNULL(g.MortgageInsurancePremium, 0)) AS MortgageInsurancePremium,
	SUM(ISNULL(bb.NetOperatingIncome, 0)) AS NetOperatingIncome
	FROM
	Lender_DataUpload_Live g
	RIGHT JOIN
	(SELECT 
		@QuarterToken AS QuarterToken,
		d.maxid,
		d.FhaLinked,
		d.Portfolio_Number,
		d.Portfolio_Name,
		CASE c.OperatorOwner
			WHEN 'Operator' THEN c.NetIncome + c.RentLeaseExpense + c.DepreciationExpense + c.AmortizationExpense
			WHEN 'Owner-Operator' THEN c.NetIncome + c.DepreciationExpense + c.AmortizationExpense + c.FHAInsuredInterestPayment + c.MortgageInsurancePremium + c.ReserveForReplacementDeposit
		END AS NetOperatingIncome
	FROM Lender_DataUpload_Live c
	RIGHT JOIN 
	(SELECT
		MAX(aa.LDP_ID) AS maxid,
		COUNT(1) AS count,
		b.Portfolio_Number,
		b.Portfolio_Name,
		b.FHANumber as FhaLinked		
	  FROM [$(DatabaseName)].[dbo].[Lender_DataUpload_Live] aa
	  RIGHT JOIN ProjectInfo b
	  ON aa.FHANumber=b.FHANumber AND dbo.fn_hcp_getquartertoken(aa.periodending, aa.monthsinperiod) = @QuarterToken
	  RIGHT JOIN Lender_FHANumber lf
	  ON b.FHANumber = lf.FHANumber
	  AND b.LenderID = lf.LenderID
	  where FHA_EndDate IS NULL
	  GROUP BY b.Portfolio_Number, b.Portfolio_Name, b.FHANumber
	  HAVING b.Portfolio_Number IS NOT NULL) d
	  ON c.FHANumber=d.FhaLinked AND c.LDP_ID = d.maxid 
	) bb
ON g.FHANumber=bb.FhaLinked AND dbo.fn_hcp_getquartertoken(g.periodending, g.monthsinperiod) = bb.QuarterToken AND bb.maxid = g.LDP_ID
GROUP BY bb.Portfolio_Number, bb.Portfolio_Name
ORDER BY bb.Portfolio_Number

