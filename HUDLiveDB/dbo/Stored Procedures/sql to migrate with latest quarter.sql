USE [HCP_Intermediate_db]
GO

BEGIN TRANSACTION
DECLARE @dtNow DATETIME
DECLARE @firstDayOfQrtRecent DATETIME
SET @dtNow = GETDATE()
SELECT @firstDayOfQrtRecent = DATEADD(quarter,DATEDIFF(quarter,0,@dtNow),0)

-- update existing rows
UPDATE [HCP_Live_db].[dbo].[Lender_DataUpload_Live]
SET 
	[ProjectName] = a.ProjectName,
	[ServiceName] = a.ServiceName,
	[OperatorOwner] = a.OperatorOwner,
	[FinancialStatementType] = a.FinancialStatementType,
	[UnitsInFacility] = a.UnitsInFacility,
	[LenderID] = a.LenderID,
	[PeriodEnding] = CAST(a.PeriodEnding AS datetime),
	[MonthsInPeriod] = CAST(a.MonthsInPeriod AS int),
	[FHANumber] = a.FHANumber,
	[PropertyID] = a.PropertyID,
	[OperatingCash] = CAST(a.OperatingCash AS decimal(19,2)),
	[Investments] = CAST(a.Investments AS decimal(19,2)),
	[ReserveForReplacementEscrowBalance] = a.ReserveForReplacementEscrowBalance,
	[AccountsReceivable] = a.AccountsReceivable,
	[CurrentAssets] = a.CurrentAssets,
	[CurrentLiabilities] = a.CurrentLiabilities,
	[TotalRevenues] = a.TotalRevenues,
	[RentLeaseExpense] = a.RentLeaseExpense,
	[DepreciationExpense] = a.DepreciationExpense,
	[AmortizationExpense] = a.AmortizationExpense,
	[TotalExpenses] = a.TotalExpenses,
	[NetIncome] = a.NetIncome,
	[ReserveForReplacementDeposit] = a.ReserveForReplacementDeposit,
	[FHAInsuredPrincipalPayment] = a.FHAInsuredPrincipalPayment,
	[FHAInsuredInterestPayment] = a.FHAInsuredInterestPayment,
	[MortgageInsurancePremium] = a.MortgageInsurancePremium,
	--[ResidentPatientDays] = a.ResidentPatientDays,
	--[OperatingBeds] = CAST(a.TotalBedsoperated AS int),
	[DateInserted] = GETUTCDATE(),		-- as DataInserted
	--a.HUD_Project_Manager_ID,
	[LDI_ID] = a.LDI_ID,
	[ReserveForReplacementBalancePerUnit] = a.ReserveForReplacementBalancePerUnit,
	[WorkingCapital] = a.WorkingCapital,
	[DebtCoverageRatio] = a.DebtCoverageRatio,
	[DaysCashOnHand] = a.DaysCashOnHand,
	[DaysInAcctReceivable] = a.DaysInAcctReceivable,
	[AvgPaymentPeriod] = a.AvgPaymentPeriod,
	[WorkingCapitalScore] = a.WorkingCapitalScore,
	[DebtCoverageRatioScore] = a.DebtCoverageRatioScore,
	[DaysCashOnHandScore] = a.DaysCashOnHandScore,
	[DaysInAcctReceivableScore] = a.DaysInAcctReceivableScore,
	[AvgPaymentPeriodScore] = a.AvgPaymentPeriodScore,
	[ScoreTotal] = a.ScoreTotal,
	[UserID] = a.UserID,	
	[ModifiedBy] = a.ModifiedBy,
	[ModifiedOn] = GETUTCDATE()
	-- to do: modifiedBy
FROM [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] a
INNER JOIN
(
	SELECT MAX(LDI_ID) AS MaxID, MAX(DataInserted) as MaxDate, PeriodEnding
	FROM [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate]
	GROUP BY LenderID, FHANumber, PeriodEnding
) b
ON a.LDI_ID = b.MaxID AND a.DataInserted = b.MaxDate 
AND a.PropertyID IS NOT NULL
INNER JOIN [HCP_Live_db].[dbo].[Lender_DataUpload_Live] c
ON b.PeriodEnding = c.PeriodEnding AND b.MaxID = c.LDI_ID
WHERE a.DataInserted >= @firstDayOfQrtRecent

-- insert for latest new rows, that don't exist on live already
INSERT INTO [HCP_Live_db].[dbo].[Lender_DataUpload_Live]
	(
	 [ProjectName]
	,[ServiceName]
	,[OperatorOwner]
	,[FinancialStatementType]
	,[UnitsInFacility]
	,[LenderID]
    --,[Servicer_ID]
    ,[PeriodEnding]
    ,[MonthsInPeriod]
    ,[FHANumber]
    ,[PropertyID]
    ,[OperatingCash]
    ,[Investments]
    ,[ReserveForReplacementEscrowBalance]
    ,[AccountsReceivable]
    ,[CurrentAssets]
    ,[CurrentLiabilities]
    ,[TotalRevenues]
    ,[RentLeaseExpense]
    ,[DepreciationExpense]
    ,[AmortizationExpense]
    ,[TotalExpenses]
    ,[NetIncome]
	,[ReserveForReplacementDeposit]
    ,[FHAInsuredPrincipalPayment]
    ,[FHAInsuredInterestPayment]
    ,[MortgageInsurancePremium]
    --,[ResidentPatientDays]
    --,[OperatingBeds]
    ,[DateInserted]
    --,[HUD_Project_Manager_ID]
	,[LDI_ID]
	,[ReserveForReplacementBalancePerUnit]
    ,[WorkingCapital]
    ,[DebtCoverageRatio]
    ,[DaysCashOnHand]
    ,[DaysInAcctReceivable]
    ,[AvgPaymentPeriod]
    ,[WorkingCapitalScore]
    ,[DebtCoverageRatioScore]
    ,[DaysCashOnHandScore]
    ,[DaysInAcctReceivableScore]
    ,[AvgPaymentPeriodScore]
    ,[ScoreTotal]
	,[UserID]
    ,[ModifiedBy]
	,[ModifiedOn]
    ,[OnBehalfOfBy]
	)
SELECT 
	a.ProjectName,
	a.ServiceName,
	a.OperatorOwner,
	a.FinancialStatementType,
	a.UnitsInFacility,
	a.LenderID,
	--a.Servicer_ID
	CAST(a.PeriodEnding AS datetime),
	CAST(a.MonthsInPeriod AS int),
	a.FHANumber,
	a.PropertyID,
	CAST(a.OperatingCash AS decimal(19,2)),
	CAST(a.Investments AS decimal(19,2)),
	a.ReserveForReplacementEscrowBalance,
	a.AccountsReceivable,
	a.CurrentAssets,
	a.CurrentLiabilities,
	a.TotalRevenues,
	a.RentLeaseExpense,
	a.DepreciationExpense,
	a.AmortizationExpense,
	a.TotalExpenses,
	a.NetIncome,
	a.ReserveForReplacementDeposit,
	a.FHAInsuredPrincipalPayment,
	a.FHAInsuredInterestPayment,
	a.MortgageInsurancePremium,
	--a.ResidentPatientDays,
	--CAST(a.TotalBedsoperated AS int),
	GETUTCDATE(),		-- as DataInserted
	--a.HUD_Project_Manager_ID,
	a.LDI_ID,
	a.ReserveForReplacementBalancePerUnit,
	a.WorkingCapital,
	a.DebtCoverageRatio,
	a.DaysCashOnHand,
	a.DaysInAcctReceivable,
	a.AvgPaymentPeriod,
	a.WorkingCapitalScore,
	a.DebtCoverageRatioScore,
	a.DaysCashOnHandScore,
	a.DaysInAcctReceivableScore,
	a.AvgPaymentPeriodScore,
	a.ScoreTotal,
	a.UserID,
	a.ModifiedBy,
	a.ModifiedOn,
	a.OnBehalfOfBy
FROM [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate] a
INNER JOIN
(
	SELECT MAX(LDI_ID) AS MaxID, MAX(DataInserted) as MaxDate, PeriodEnding
	FROM [HCP_Intermediate_db].[dbo].[Lender_DataUpload_Intermediate]
	GROUP BY LenderID, FHANumber, PeriodEnding
) b
ON a.LDI_ID = b.MaxID AND a.DataInserted = b.MaxDate 
AND a.PropertyID IS NOT NULL
LEFT JOIN [HCP_Live_db].[dbo].[Lender_DataUpload_Live] c
ON b.PeriodEnding = c.PeriodEnding AND b.MaxID = c.LDI_ID
WHERE a.DataInserted >= @firstDayOfQrtRecent AND c.LDI_ID IS NULL

IF (@@ERROR <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndMigration
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndMigration:

GO

