﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class Prod_LoanCommitteeManager : IProd_LoanCommitteeManager
    {
        private readonly UnitOfWork unitOfWork;
        private IProd_LoanCommitteeRepository loanCommitteRepository;
        private IUserInRoleRepository userInRoleReposity;
        public Prod_LoanCommitteeManager()
        {
            unitOfWork = new UnitOfWork(DBSource.Live);
            loanCommitteRepository = new Prod_LoanCommitteeRepository(unitOfWork);
            userInRoleReposity = new UserInRoleRepository(unitOfWork);
        }

        public Prod_LoanCommitteeViewModel GetLoanCommitteeDetails(Guid applicationTaskInstanceId)
        {
            return loanCommitteRepository.GetLoanCommitteeDetails(applicationTaskInstanceId);
        }

        public Guid SaveLoanCommitteeDetails(Prod_LoanCommitteeViewModel lcModel)
        {
            return loanCommitteRepository.SaveLoanCommitteeDetails(lcModel);
        }

        public Prod_LoanCommitteeViewModel GetLoanCommitteeDetailByFha(string pFHANumber)
        {
            return loanCommitteRepository.GetLoanCommitteeDetailByFha(pFHANumber);
        }

        public IList<Prod_LoanCommitteeViewModel> GetLCFiltersValues()
        {
            return loanCommitteRepository.GetLCFiltersValues();
        }
        public IList<Prod_LoanCommitteeViewModel> UpdateLCGridResults(IList<Prod_LoanCommitteeViewModel> Model)
        {
            return loanCommitteRepository.UpdateLCGridResults(Model);
        }
        public IList<string> AllProductionUsers()
        {
            return loanCommitteRepository.AllProductionUsers();
        }


        public int GetLCDateCount(DateTime lcDate)
        {
            return loanCommitteRepository.GetLCDateCount(lcDate);
        }

        public IList<string> GetLoanCommitteeDatesToDisable()
        {
            return loanCommitteRepository.GetLoanCommitteeDatesToDisable();
        }

        public bool IsUserProductionWlm(int userId)
        {
            return userInRoleReposity.IsUserProductionWlm(userId);
        }
        public List<Prod_LoanCommitteeViewModel> GetSubmitedDates(Guid applicationTaskInstanceId)
        {
            return loanCommitteRepository.GetSubmitedDates(applicationTaskInstanceId);
        }
        public Prod_LoanCommitteeViewModel GetLoanCommitteeSelctedDateInfo(Guid taskInstanceId, string SelectedDate)
        {
            return loanCommitteRepository.GetLoanCommitteeSelctedDateInfo( taskInstanceId, SelectedDate);
        }
    }
}
