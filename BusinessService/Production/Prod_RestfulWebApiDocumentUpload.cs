﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System.Web;
namespace BusinessService.Production
{
    public class Prod_RestfulWebApiDocumentUpload : IProd_RestfulWebApiDocumentUpload
    {


        private IProd_RestfulWebApiUploadRepository WebApiDocumentUploadRepository;
        public Prod_RestfulWebApiDocumentUpload()
        {

            WebApiDocumentUploadRepository = new Prod_RestfulWebApiUploadRepository();
        }

        public RestfulWebApiResultModel UploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType)
        {
            return WebApiDocumentUploadRepository.UploadDocumentUsingWebApi(file, token, myFile, requestType);
        }
        public RestfulWebApiResultModel AssetManagementUploadDocumentUsingWebApi(RestfulWebApiUploadModel file, string token, HttpPostedFileBase myFile, string requestType)
        {
            return WebApiDocumentUploadRepository.AssetManagementUploadDocumentUsingWebApi(file, token, myFile, requestType);
        }

        public RestfulWebApiResultModel uploadSharepointPdfFile(RestfulWebApiUploadModel documentInfo, string token, byte[] binary)
        {
            return WebApiDocumentUploadRepository.uploadSharepointPdfFile(documentInfo, token, binary);
        }
        public RestfulWebApiResultModel uploadCopiedFile(RestfulWebApiUploadModel documentInfo, string token, byte[] binary, string filename)
        {
            return WebApiDocumentUploadRepository.uploadCopiedFile(documentInfo, token, binary, filename);
        }

    }
}
