﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using HUDHealthcarePortal.Model;

namespace BusinessService.Production
{
    public class Prod_GroupTasksManager:IProd_GroupTasksManager
    {
        private UnitOfWork unitOfWorkTask;
        private IProd_GroupTasksRepository groupTaskRepository;

        public Prod_GroupTasksManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
            groupTaskRepository = new Prod_GroupTasksRepository(unitOfWorkTask);
        }
        public Guid AddProdGroupTasks(Prod_GroupTasksModel model)
        {
            return groupTaskRepository.AddProdGroupTasks(model);
        }

        public void UpdateGroupTask(Prod_GroupTasksModel model)
        {
            groupTaskRepository.UpdateGroupTask(model);
        }

        public bool IsGroupTaskAvailable(Guid taskInstanceId)
        {
            return groupTaskRepository.IsGroupTaskAvailable(taskInstanceId);
        }

        public Prod_GroupTasksModel GetGroupTaskByTaskInstanceId(Guid taskInstanceId)
        {
            return groupTaskRepository.GetGroupTaskByTaskInstanceId(taskInstanceId);
        }





        public Prod_GroupTasksModel GetGroupTaskAByTaskInstanceId(Guid TaskInstanceId)
        {
            return groupTaskRepository.GetGroupTaskAByTaskInstanceId(TaskInstanceId);
             
        }


        public void UpdateProdGroupTask(OPAViewModel projectActionViewModel)
        {
             groupTaskRepository.UpdateProdGroupTask(projectActionViewModel);
        }

        public void Checkin(OPAViewModel AppProcessModel)
        {
            groupTaskRepository.Checkin(AppProcessModel);
        }


        public List<Prod_GroupTasksModel> GetGroupTask(int userid)
        {
            return groupTaskRepository.GetGroupTask(userid);
        }


        public void UnlockGroupTask(int taskId)
        {
            groupTaskRepository.UnlockGroupTask(taskId);
        }

        public Prod_GroupTasksModel GetGroupTaskAById(int TaskId)
        {
            return groupTaskRepository.GetGroupTaskAById(TaskId);
        }
    }
}
