﻿using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class Prod_AdhocDataManager : IProd_AdhocDataManager
    {
        private IProd_AdhocDataRepository adhocDataRepository;
        private readonly UnitOfWork unitOfWork;

        public Prod_AdhocDataManager()
        {
            unitOfWork = new UnitOfWork(DBSource.Live);
            adhocDataRepository = new Prod_AdhocDataRepository(unitOfWork);

        }

        public IList<Prod_AdhocDataModel> GetProdDataForAdhocDataDownload()
        {
            return adhocDataRepository.GetProdDataForAdhocDataDownload();
        }
    }
}

  