﻿using HUDHealthcarePortal.Core;
using Model;

namespace BusinessService.Interfaces
{
    public interface IManagementReportManager
    {
        ReportModel GetHighRiskProjectReport(HUDRole hudRole);
    }
}
