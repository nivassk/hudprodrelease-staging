﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model.ProjectAction;
using Model;

namespace BusinessService.Interfaces
{
    public interface IQuestionManager
    {
        void AddQuestion(QuestionViewModel  questionViewModel);
        IList<QuestionViewModel> GetAllQuestions(int projectActionTypeId);
        void UpdateQuestion(QuestionViewModel questionViewModel);
        QuestionViewModel GetQuestion(Guid  checkListId);
    }
}
