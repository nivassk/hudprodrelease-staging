﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;
using Model;

namespace BusinessService.Interfaces
{
    public interface ITaskReAssignmentManager
    {
        IEnumerable<KeyValuePair<string, string>> GetAllTaskAgeIntervals();

        PaginateSortModel<TaskDetailPerAeModel> GetTaskDetailsForAE(string aeIds, string fromDate, string toDate,
            string taskAge, string projectAction, int? page, string sort, SqlOrderByDirecton sortDir);

        PaginateSortModel<TaskDetailPerAeModel> ReassignAE(string aeId, string reassignAeId, string reassignISOUId,
            string selectedTaskIds, string fromDate, string toDate, string taskAge,
            string projectAction, int page, string sort, SqlOrderByDirecton sortDir);

        IList<HUDManagerModel> GetRegisteredAEsByWLMId(string wlmId);

        IList<HUDManagerModel> GetISOUIdsByWLMId(string wlmId);

        UserViewModel GetAccountExecutiveDetail(int id);

        IList<TaskDetailPerAeModel> GetReassignedTasksForAE(string reAssignedTo, string selectedTaskIds);

        string GetAENameById(string aeId);

        string GetISOUNameById(int isouId);

        string GetISOUEmailById(int isouId);

        List<string> GetInvalidTaskFHAsForIsou(int isouId, List<string> instanceIds);

        Boolean IsAllTaskNCRE(List<string> instanceIds);

        string GetWorkloadManagerEmailFromAeId(int aeId);

        PaginateSortModel<TaskReAssignmentViewModel> GetReAssignedTasksByUserName(string userName, int page,
            string sort, SqlOrderByDirecton sortDir);

        string GetReassignedAE(Guid TaskInstanceId);

        string GetWorkloadManagerEmailFromInternalSpecialOptionUserId(int isouId);

        int GetWorkloadManagerIdFromInternalSpecialOptionUserId(int isouId);
    }
}
