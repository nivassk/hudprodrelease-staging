﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace BusinessService.Interfaces
{
    public interface IMultiLoanManager
    {
        IEnumerable<MultiLoanModel> GetMultiLoans();
        IEnumerable<MultiLoanDetailModel> GetMultiLoansDetailByPropertyID(int propertyId);
    }
}
