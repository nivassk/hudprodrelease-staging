﻿using HUDHealthcarePortal.Core;
using Model;
using System;
using System.Collections.Generic;

namespace BusinessService.Interfaces
{
    public interface IEmailTemplateMessagesManager
    {
        EmailTemplateMessagesModel GetEmailTemplate(EmailType emailTypeID);
        EmailRecipientModel GetRecipentbyView(Guid Taskinstanceid, int viewid);
        IEnumerable<string> GetAeEmailbyInstanceid(Guid Taskinstanceid);
    }
}
