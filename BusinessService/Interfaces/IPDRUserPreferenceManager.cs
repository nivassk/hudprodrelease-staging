﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace BusinessService.Interfaces
{
    public interface IPDRUserPreferenceManager
    {
        int AddPDRUserPreference(PDRUserPreferenceViewModel model);
        void UpdatePDRUserPreference(PDRUserPreferenceViewModel model);
        IEnumerable<PDRUserPreferenceViewModel> GetUserPreference(int userID);
        IEnumerable<PDRColumnViewModel> GetAllColumns();
        void DeletePDRUserPreference(int userID, int pdrColumnID);
        void SetDefaultUserPreference(int userID);

    }
}
