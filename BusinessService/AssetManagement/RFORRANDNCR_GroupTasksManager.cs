﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces;
using Repository.Interfaces.AssetManagement;
using Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Core;
using Repository.AssetManagement;

namespace BusinessService.AssetManagement
{
    public class RFORRANDNCR_GroupTasksManager : IRFORRANDNCR_GroupTasksManager
    {
       private UnitOfWork unitOfWorkTask;
        private IRFORRANDNCR_GroupTasksRepository rFORRANDNCR_GroupTasksRepository;

        public RFORRANDNCR_GroupTasksManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
           
            rFORRANDNCR_GroupTasksRepository = new RFORRANDNCR_GroupTasksRepository(unitOfWorkTask);
            

        }
        public RFORRANDNCR_GroupTaskModel GetGroupTaskAByTaskInstanceId(Guid TaskInstanceId)
        {
            return rFORRANDNCR_GroupTasksRepository.GetGroupTaskAByTaskInstanceId(TaskInstanceId);

        }
        public Guid AddR4RGroupTasks(RFORRANDNCR_GroupTaskModel model)
        {
            return rFORRANDNCR_GroupTasksRepository.AddR4RGroupTasks(model);
        }
        
        public void DeleteGroupTask(Guid TaskInstanceId, string FHANumber)
        {
            rFORRANDNCR_GroupTasksRepository.DeleteGroupTask(TaskInstanceId, FHANumber);
        }
    }
}
