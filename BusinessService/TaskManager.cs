﻿using System.Threading.Tasks;
using BusinessService.Interfaces;
using Core;
using HUDHealthcarePortal.BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService.ProjectAction;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using Repository;
using Repository.Interfaces;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;
using System.Linq;
using HUDHealthcarePortal.Repository.Interfaces;
using Repository.Interfaces.OPAForm;
using Repository.Interfaces.ProjectAction;
using Repository.OPAForm;
using Repository.ProjectAction;
using EntityObject.Entities.HCP_live;
using TaskDB = EntityObject.Entities.HCP_task;
using Model.Production;


namespace HUDHealthcarePortal.BusinessService
{
    public class TaskManager : ITaskManager
    {
        private ITaskRepository taskRepository;
        private ITaskConcurrencyRepository taskConcurrencyRepository;
        private ITaskFileRepository taskFileRepository;
        private ISubFolderStructureRepository subFolderStructureRepository;
        private IUserRepository userRepository;
        private IProjectActionFormRepository projectActionFormRepository;
        private IPageTypeRepository pageTypeRepository;
        private IOPARepository opaRepository;
        private IProjectInfoRepository projectInfoRepository;
        private IParentChildTaskRepository parentChildTaskRepository;
        private UnitOfWork unitOfWorkLive;
        private UnitOfWork unitOfWorkTask;
        private IAddressRepository addressRepository;
        private IReviewerTitlesRepository reviewerTitlesRepository;

        public TaskManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            userRepository = new UserRepository(unitOfWorkLive);
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
            taskRepository = new TaskRepository(unitOfWorkTask);
            taskFileRepository = new TaskFileRepository(unitOfWorkTask);
            subFolderStructureRepository = new SubFolderStructureRepository(unitOfWorkTask);
            taskConcurrencyRepository = new TaskConcurrencyRepository(unitOfWorkTask);
            projectActionFormRepository = new ProjectActionFormRepository(unitOfWorkLive);
            pageTypeRepository = new PageTypeRepository(unitOfWorkLive);
            opaRepository = new OPARepository(unitOfWorkLive);
            projectInfoRepository = new ProjectInfoRepository(unitOfWorkLive);
            parentChildTaskRepository = new ParentChildTaskRepository(unitOfWorkTask);
            addressRepository = new AddressRepository(unitOfWorkLive);
            reviewerTitlesRepository = new ReviewerTitlesRepository(unitOfWorkLive);
        }

        public void UpdateTaskNotes(int TaskId, string HudRemarks)
        {
            taskRepository.UpdateTaskNotes(TaskId, HudRemarks);
            unitOfWorkLive.Save();
        }

        public void UpdateDataStore1(int TaskId, string dataStoreXML)
        {
            taskRepository.UpdateDataStore1(TaskId, dataStoreXML);
            unitOfWorkLive.Save();

        }

        public TaskFileModel GetTaskFileByTaskInstanceId(Guid taskInstanceId)
        {
            var taskFileList = taskFileRepository.GetTaskFileByTaskInstanceId(taskInstanceId);
            if (taskFileList.Any())
            {
                return taskFileList.OrderByDescending(p => p.UploadTime).ToList().First<TaskFileModel>();
            }
            return null;
        }
        public TaskFileModel GetTaskFileByTaskFileId(Guid taskFileId)
        {
            var taskFileList = taskFileRepository.GetTaskFileByTaskFileId(taskFileId);
            if (taskFileList.Any())
            {
                return taskFileList.OrderByDescending(p => p.UploadTime).ToList().First<TaskFileModel>();
            }
            return null;
        }
        
            public TaskFileModel GetTaskFileByTaskFileName(string FileName)
        {
            var taskFile = taskFileRepository.GetTaskFileByTaskFileName(FileName);
            if (taskFile.Any())
            {
                return taskFile.First();
            }
            return null;
        }

        public TaskFileModel GetTaskFileByTaskFileType(string TaskFileType)
        {
            var taskFile = taskFileRepository.GetTaskFileByTaskFileType(TaskFileType);
            if (taskFile.Any())
            {
                return taskFile.First();
            }
            return null;
        }
        public TaskFileModel GetTaskFileByTaskInstanceAndFileType(Guid TaskInstanceId, string FileType)
        {
           return taskFileRepository.GetTaskFileByTaskInstanceAndFileType( TaskInstanceId,  FileType);            
        }
        public TaskFileModel GetTaskFileByTaskInstanceAndFileTypeId(Guid taskInstanceId, FileType fileType)
        {
            var taskFileList = taskFileRepository.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId, fileType);
            if (taskFileList.Any())
            {
                return taskFileList.OrderByDescending(p => p.UploadTime).ToList().First<TaskFileModel>();
            }
            return null;
        }
        // Relieve the user of handling retrieval from within a null returned object.
        public string GetTaskFileNameByTaskInstanceId(Guid taskInstanceId)
        {
            var taskFileList = taskFileRepository.GetTaskFileByTaskInstanceId(taskInstanceId);
            if (taskFileList.Any())
            {
                return taskFileList.OrderByDescending(p => p.UploadTime).ToList().First<TaskFileModel>().FileName;
            }
            return null;
        }

        public IEnumerable<TaskFileModel> GetAllTaskFileByTaskInstanceId(Guid taskInstanceId)
        {
            return taskFileRepository.GetTaskFileByTaskInstanceId(taskInstanceId);
        }

        public TaskFileModel GetTaskFileById(int taskFileId)
        {
            return taskFileRepository.GetTaskFileById(taskFileId);
        }

        public void SaveTask(List<TaskModel> taskList, TaskFileModel fileModel)
        {
            if (taskList[0].Concurrency == null)
            {
                taskConcurrencyRepository.AddTaskInstance(taskList[0]);
            }
            else
            {
                taskConcurrencyRepository.UpdateTaskInstance(taskList[0]);
            }

            if (fileModel != null && !string.IsNullOrEmpty(fileModel.FileName))
            {
                if (GetTaskFileNameByTaskInstanceId(taskList[0].TaskInstanceId) == null)
                {
                    fileModel.TaskFileId = Guid.NewGuid();
                    taskFileRepository.AddTaskFile(fileModel);
                }
                else
                {
                    taskFileRepository.UpdateTaskFile(fileModel);
                }
            }

            foreach (var task in taskList)
            {
                taskRepository.AddTask(task);
            }

            unitOfWorkTask.Save();
        }

        public void SaveInternalExternalTask(TaskModel task)
        {
            taskRepository.AddTask(task);
            unitOfWorkTask.Save();
        }
        public void SaveTask(List<TaskModel> taskList, IList<TaskFileModel> lstFileModels)
        {

            if (taskList[0].Concurrency == null)
            {
                taskConcurrencyRepository.AddTaskInstance(taskList[0]);
            }
            else
            {
                taskConcurrencyRepository.UpdateTaskInstance(taskList[0]);
            }

            lstFileModels.ToList().ForEach(f =>
            {
                if (GetTaskFileByTaskInstanceAndFileTypeId(taskList[0].TaskInstanceId, f.FileType) == null)
                {
                    f.TaskFileId = Guid.NewGuid();
                    taskFileRepository.AddTaskFile(f);
                }
                else
                {
                    taskFileRepository.UpdateTaskFile(f);
                }
            });

            foreach (var task in taskList)
            {
                taskRepository.AddTask(task);
            }

            unitOfWorkTask.Save();
        }

        public PaginateSortModel<TaskModel> GetTasksByUserName(string userName, int page, string sort, SqlOrderByDirecton sortDir)
        {
            var results = taskRepository.GetTasksByUserName(userName).ToList().Where(a => a.PageTypeId < 4 || a.PageTypeId == null);
            // results = results.Where(a => a.PageTypeId < 4);
            foreach (var item in results)
            {
                var taskType = EnumType.Parse<TaskStep>(item.TaskStepId.ToString());
                if (UserPrincipal.Current.UserRole == "Reviewer")
                {
                    string email = UserPrincipal.Current.UserName;
                    var useraddress = addressRepository.GetAddressByUserName(email);
                    int titleId = Convert.ToInt32(useraddress.Title);
                    var titles = reviewerTitlesRepository.GetReviewerTitle(titleId);
                    item.TaskName = "(" + titles.Reviewer_Name + ")" + EnumType.GetEnumDescription(taskType);
                    item.AssignedTo = "(" + titles.Reviewer_Name + ")" + item.AssignedTo;
                }
                else
                {
                    item.TaskName = EnumType.GetEnumDescription(taskType);
                }
                if (item.DataStore1 != null && item.DataStore1.Contains("NonCriticalRequestExtensionModel"))
                {
                    var formUploadData =
                            XmlHelper.Deserialize(typeof(NonCriticalRequestExtensionModel), item.DataStore1,
                                    Encoding.Unicode)
                            as
                            NonCriticalRequestExtensionModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                else if (item.DataStore1 != null && item.DataStore1.Contains("NonCriticalRepairsViewModel"))
                {
                    var formUploadData =
                            XmlHelper.Deserialize(typeof(NonCriticalRepairsViewModel), item.DataStore1, Encoding.Unicode)
                            as
                            NonCriticalRepairsViewModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                else if (item.DataStore1 != null && item.DataStore1.Contains("ReserveForReplacementFormModel"))
                {
                    var formUploadData =
                            XmlHelper.Deserialize(typeof(ReserveForReplacementFormModel), item.DataStore1,
                                    Encoding.Unicode)
                            as
                            ReserveForReplacementFormModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                else if (item.DataStore1 != null && item.DataStore1.Contains("ProjectActionViewModel"))
                {
                    var formUploadData =
                        XmlHelper.Deserialize(typeof(ProjectActionViewModel), item.DataStore1, Encoding.Unicode)
                            as
                            ProjectActionViewModel;
                    if (formUploadData != null)
                    {
                        var projectActionName =
                            projectActionFormRepository.GetProjectActionName(formUploadData.ProjectActionTypeId);
                        item.TaskName = item.TaskName.Replace("Project Action Request", projectActionName);
                        item.ItemName = formUploadData.PropertyName;
                        item.FHANumber = formUploadData.FhaNumber;
                    }
                }
                else if (item.PageTypeId == pageTypeRepository.GetPageTypeIdByName("OPA"))
                {
                    var formUploadData = opaRepository.GetOPAByTaskId(item.TaskId);
                    if (formUploadData != null)
                    {
                        var projectActionName =
                            projectActionFormRepository.GetProjectActionName(formUploadData.ProjectActionTypeId);
                        item.TaskName = item.TaskName.Replace("Project Action Request", projectActionName);
                    }
                    else
                    {
                        if (parentChildTaskRepository.IsParentTaskAvailable(item.TaskInstanceId))
                        {
                            formUploadData = opaRepository.GetOPAByChildTaskId(item.TaskInstanceId);
                            var projectActionName = (dynamic)null; ;
                            if (formUploadData != null)
                            {
                                 projectActionName =
                                projectActionFormRepository.GetProjectActionName(formUploadData.ProjectActionTypeId);
                            }
                            item.TaskName = item.TaskName.Replace("Project Action Request", projectActionName);
                        }
                    }
                    item.ItemName = projectInfoRepository.GetProjectInfoByFhaNumber(item.FHANumber).ProjectName;
                }
                else if (item.PageTypeId == pageTypeRepository.GetPageTypeIdByName("APPLICATION REQUEST"))
                {
                    var formUploadData = opaRepository.GetOPAByTaskId(item.TaskId);
                    if (formUploadData != null)
                    {
                        var projectActionName =
                            projectActionFormRepository.GetProjectActionName(formUploadData.ProjectActionTypeId);
                        item.TaskName = item.TaskName.Replace("Project Action Request", projectActionName);
                    }
                    else
                    {
                        if (parentChildTaskRepository.IsParentTaskAvailable(item.TaskInstanceId))
                        {
                            formUploadData = opaRepository.GetProdOPAByChildTaskId(item.TaskInstanceId);
                            var projectActionName =
                            projectActionFormRepository.GetProjectActionName(formUploadData.ProjectActionTypeId);
                            item.TaskName = item.TaskName.Replace("Project Action Request", projectActionName);
                        }
                    }

                    item.ItemName = projectInfoRepository.GetProjectInfoByFhaNumber(item.FHANumber).ProjectName;
                }

                else if (item.DataStore1 != null && item.DataStore1.Contains("FormUploadModel"))
                {
                    var formUploadData =
                        XmlHelper.Deserialize(typeof(FormUploadModel), item.DataStore1, Encoding.Unicode) as
                            FormUploadModel;
                    if (formUploadData != null)
                    {
                        item.ItemName = formUploadData.ProjectName;
                        item.FHANumber = formUploadData.FHANumber;
                    }
                }
                if (item.ItemName == null) item.ItemName = item.PropertyName;
                if (item.FHANumber == null) item.FHANumber = item.FHANumber;
                item.MyStartTime = TimezoneManager.GetPreferredTimeFromUtc(item.StartTime);
            }
            SetStatusForTasks(results);
            var model = new PaginateSortModel<TaskModel>();
            model.Entities = results.ToList();
            model.PageSize = 10;
            model.TotalRows = results.ToList().Count();
            var query = model.Entities.AsQueryable();
            return PaginateSort.SortAndPaginate(query, sort, sortDir, model.PageSize, page);
        }

        public static void SetStatusForTasks(IEnumerable<TaskModel> results)
        {
            foreach (var taskModel in results)
            {
                if (taskModel.TaskStepId == (int)TaskStep.Final)
                    taskModel.Status = FormUploadStatus.Done;
                else if (taskModel.AssignedTo != null && taskModel.AssignedTo.ToLower() != UserPrincipal.Current.UserName.ToLower())
                {
                    if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete && RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete && taskModel.SequenceId == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && taskModel.SequenceId == 1)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else
                    {
                        taskModel.Status = FormUploadStatus.AssignedToOther;
                        //taskModel.Status = FormUploadStatus.Done;
                    }

                }
                else
                {
                    if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)) && taskModel.SequenceId == 1)
                    {
                        taskModel.TaskStepId = (int)TaskStep.R4RRequest;
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)) && taskModel.SequenceId == 1)
                    {
                        taskModel.TaskStepId = (int)TaskStep.NonCriticalRequest;
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)) && (taskModel.SequenceId % 2) != 0)
                    {
                        //taskModel.TaskStepId = (int)TaskStep.ProjectActionRequest;
                        //taskModel.Status = FormUploadStatus.AssignedByOther;
                        taskModel.Status = FormUploadStatus.Done;

                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete && !(RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo)) && taskModel.SequenceId == 1)
                    {
                        taskModel.TaskStepId = (int)TaskStep.NonCriticalRequestExtension;
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRepairComplete && (taskModel.SequenceId == 2 || taskModel.SequenceId == 0))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.R4RComplete && (taskModel.SequenceId == 2 || taskModel.SequenceId == 0))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && (taskModel.SequenceId % 2) == 0)
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.NonCriticalRequestExtensionComplete && (taskModel.SequenceId == 2 || taskModel.SequenceId == 0))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else if (taskModel.TaskStepId == (int)TaskStep.ProjectActionRequestComplete && RoleManager.IsAccountExecutiveRole(taskModel.AssignedTo) && taskModel.SequenceId == 1 && taskModel.TaskOpenStatus.Contains(UserPrincipal.Current.UserName))
                    {
                        taskModel.Status = FormUploadStatus.Done;
                    }
                    else
                    {
                        taskModel.Status = FormUploadStatus.AssignedByOther;
                    }
                }
            }
        }

        public IEnumerable<TaskModel> GetTasksByTaskInstanceId(Guid taskInstanceId)
        {
            var results =
             taskRepository.GetTasksByTaskInstanceId(taskInstanceId).ToList();
            SetStatusForTasks(results);
            return results;
        }

        public byte[] GetConcurrencyTimeStamp(Guid taskInstanceId)
        {
            return taskConcurrencyRepository.GetConcurrencyTimeStamp(taskInstanceId);
        }

        public int GetMyUnOpenedTaskCount()
        {
            var myTasks = taskRepository.GetTasksByUserName(UserPrincipal.Current.UserName);
            myTasks = myTasks.Where(a => a.PageTypeId < 4 || a.PageTypeId == null);
            int count = 0;
            foreach (var task in myTasks)
            {
                if (RoleManager.IsAccountExecutiveRole(UserPrincipal.Current.UserName) &&
                    task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower() &&
                    (task.TaskStepId == 4 || task.TaskStepId == 5 || task.TaskStepId == 12 || task.TaskStepId == 14) &&
                    (task.DataStore1 != null && task.DataStore1.Contains("NonCriticalRepairsViewModel") ||
                     task.DataStore1 != null && task.DataStore1.Contains("NonCriticalRequestExtensionModel") ||
                     task.DataStore1 != null && task.DataStore1.Contains("ReserveForReplacementFormModel") ||
                     (task.PageTypeId == pageTypeRepository.GetPageTypeIdByName("OPA"))
                    )
                )
                {
                    count++;
                }
                else if (RoleManager.IsCurrentUserLenderRoles() &&
                   (task.TaskStepId == 3 && task.AssignedTo != null && task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower()))
                {
                    count++;
                }
                else if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) &&
                    task.AssignedTo != null && task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower() &&
                    (task.TaskStepId == 6 || task.TaskStepId == 7 || task.TaskStepId == 13 || task.TaskStepId == 14 || task.TaskStepId == 15) &&
                    (task.DataStore1 != null && task.DataStore1.Contains("NonCriticalRepairsViewModel") ||
                     task.DataStore1 != null && task.DataStore1.Contains("NonCriticalRequestExtensionModel") ||
                     task.DataStore1 != null && task.DataStore1.Contains("ReserveForReplacementFormModel") ||
                     (task.PageTypeId == pageTypeRepository.GetPageTypeIdByName("OPA"))
                        )
                     )
                {
                    var taskOpenStatus =
                     XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                         TaskOpenStatusModel;
                    if (taskOpenStatus != null &&
                        !taskOpenStatus.TaskOpenStatus.Exists(p => p.Key.ToLower().Equals(UserPrincipal.Current.UserName.ToLower())))
                        count++;

                }
                else if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) &&
                    task.AssignedTo != null && task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower() &&
                (task.TaskStepId == 16) &&
                (task.PageTypeId == pageTypeRepository.GetPageTypeIdByName("OPA")))
                {
                    count++;
                }
                else if (RoleManager.IsCurrentUserLenderRoles() &&
                 (task.TaskStepId == 2 && task.AssignedTo != null && task.AssignedTo.ToLower() == UserPrincipal.Current.UserName.ToLower()))
                {
                    count++;
                }
                else if (RoleManager.IsCurrentUserLenderRoles() && !string.IsNullOrEmpty(task.TaskOpenStatus) && (task.TaskStepId == 6 || task.TaskStepId == 7 || task.TaskStepId == 13 || task.TaskStepId == 15))
                {
                }
            }
            return count;
        }

        public void UpdateTask(TaskModel task)
        {
            taskRepository.UpdateTask(task);
            unitOfWorkTask.Save();
        }

        public TaskModel GetTaskById(int taskId)
        {
            var results =
             taskRepository.GetTaskById(taskId);
            if (results.TaskStepId == (int)TaskStep.Final)
                results.Status = FormUploadStatus.Done;
            else if (results.AssignedTo != UserPrincipal.Current.UserName)
                results.Status = FormUploadStatus.AssignedToOther;
            else
                results.Status = FormUploadStatus.AssignedByOther;
            return results;

        }

        /// <summary>
        /// this method latest task id, based on start time
        /// </summary>
        /// <param name="taskInstanceId"></param>
        /// <returns></returns>
        public TaskModel GetLatestTaskByTaskInstanceId(Guid taskInstanceId)
        {
            return taskRepository.GetLatestTaskByTaskInstanceId(taskInstanceId);
        }

        public void SetModelWithFileAttachmentsForR4R(ReserveForReplacementFormModel model)
        {
            if (model.TaskGuid != null)
            {
                var taskFileModel9250 = GetTaskFileByTaskInstanceAndFileTypeId(model.TaskGuid.Value,
                    FileType.File9250);
                if (taskFileModel9250 != null)
                {
                    model.FileName9250 = taskFileModel9250.FileName;
                }

                var taskFileModel9250A = GetTaskFileByTaskInstanceAndFileTypeId(model.TaskGuid.Value,
                    FileType.File9250A);
                if (taskFileModel9250A != null)
                {
                    model.FileName9250A = taskFileModel9250A.FileName;
                }
                var taskFileModelInvoice = GetTaskFileByTaskInstanceAndFileTypeId(model.TaskGuid.Value,
                    FileType.Invoice);
                if (taskFileModelInvoice != null)
                {
                    model.FileNameInvoices = taskFileModelInvoice.FileName;
                }
                var taskFileModelContract = GetTaskFileByTaskInstanceAndFileTypeId(model.TaskGuid.Value,
                    FileType.Contract);
                if (taskFileModelContract != null)
                {
                    model.FileNameContracts = taskFileModelContract.FileName;
                }
                var taskFileModelReciept = GetTaskFileByTaskInstanceAndFileTypeId(model.TaskGuid.Value,
                    FileType.Receipt);
                if (taskFileModelReciept != null)
                {
                    model.FileNameReceipts = taskFileModelReciept.FileName;
                }
                var taskFileModelPicture = GetTaskFileByTaskInstanceAndFileTypeId(model.TaskGuid.Value,
                    FileType.Picture);
                if (taskFileModelPicture != null)
                {
                    model.FileNamePictures = taskFileModelPicture.FileName;
                }
                var taskFileModelOther = GetTaskFileByTaskInstanceAndFileTypeId(model.TaskGuid.Value,
                    FileType.Other);
                if (taskFileModelOther != null)
                {
                    model.FileNameOthers = taskFileModelOther.FileName;
                }
            }
        }

        public Guid SaveGroupTaskFileModel(TaskFileModel taskFileModel)
        {
            taskFileModel.TaskFileId = Guid.NewGuid();
            taskFileRepository.AddGroupTaskFile(taskFileModel);
            unitOfWorkTask.Save();
            return taskFileModel.TaskFileId;
        }

        public TaskFileModel GetGroupTaskFileByTaskInstanceAndFileTypeId(Guid taskInstanceId, Guid fileType)
        {
            return taskFileRepository.GetGroupTaskFileByTaskInstanceAndFileTypeId(taskInstanceId, fileType);
        }

        public int DeleteGroupTaskFile(Guid taskInstanceId, Guid fileType)
        {
            int sucess = taskFileRepository.DeleteGroupTaskFile(taskInstanceId, fileType);
            if (sucess == 1)
                unitOfWorkTask.Save();
            return sucess;
        }


        public TaskFileModel GetGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID)
        {
            return taskFileRepository.GetGroupTaskFileByTaskInstanceAndTaskFileID(taskFileId, taskInstanceID);

        }

        public int DeleteGroupTaskFileByTaskInstanceAndTaskFileID(Guid taskFileId, Guid taskInstanceID)
        {
            int sucess = taskFileRepository.DeleteGroupTaskFileByTaskInstanceAndTaskFileID(taskFileId, taskInstanceID);
            if (sucess == 1)
                unitOfWorkTask.Save();
            return sucess;
        }


        public int DeleteTaskFileByFileID(int FileID)
        {
            int sucess = taskFileRepository.DeleteTaskFileByFileID(FileID);
            if (sucess == 1)
                unitOfWorkTask.Save();
            return sucess; ;
        }
        public int DeleteSubFolderByID(int ID)
        {
            int sucess = subFolderStructureRepository.DeleteSubFolderByID(ID);
            if (sucess == 1)
                unitOfWorkTask.Save();
            return sucess; ;
        }

        public void UpdateTaskFile(TaskFileModel taskFile)
        {
            taskFileRepository.UpdateTaskFile(taskFile);
        }

      

        public void SaveReviewFileStatus(Guid taskInstanceID, int reviewerUserId, int userId, int? ReviewerProdViewId = null)
        {
            taskRepository.SaveReviewFileStatus(taskInstanceID, reviewerUserId, userId, ReviewerProdViewId);

        }
        

        public void RenameFiles(List<Tuple<Guid, string, string>> fileRenameList)
        {
            taskFileRepository.RenameFiles(fileRenameList);
        }

        public int GetReviewerUserIdByTaskInstanceId(Guid taskInstanceId)
        {
            return taskRepository.GetReviewerUserIdByTaskInstanceId(taskInstanceId);
        }
        public int CreateFHARequestTasks(string assignedBy, Guid? taskInstanceID, int userId, string userRoleName, bool isPortfolioRequired, bool isCreditReviewRequired)
        {
            return taskRepository.CreateFHARequestTasks(assignedBy, taskInstanceID, userId, userRoleName, isPortfolioRequired, isCreditReviewRequired);
        }


        #region Production Application

        public Prod_TaskXrefModel GetReviewerViewIdByXrefTaskInstanceId(Guid xrefTaskInstanceId)
        {
            return taskRepository.GetReviewerViewIdByXrefTaskInstanceId(xrefTaskInstanceId);
        }

        public int CheckAmendmentExist(string selectedFhaNumber, int PageTypeId, int ViewId)
        {
            return taskRepository.CheckAmendmentExist( selectedFhaNumber,  PageTypeId,  ViewId);
        }
        public TaskModel GetLatestTaskByTaskXrefid(Guid taskXrefId)
        {
            return taskRepository.GetLatestTaskByTaskXrefid(taskXrefId);
        }
        public int GetFolderKeyForChildFileId(Guid ParentTaskFileId)
        {
            return taskRepository.GetFolderKeyForChildFileId(ParentTaskFileId);

    }


        #endregion


        public void UpdateTaskAssignment(TaskModel task)
        {
            taskRepository.UpdateTaskAssignment(task);
        }

        public void UpdateDocType(TaskFileModel task)
        {
            taskFileRepository.UpdateDocType(task);
        }

        public IList<TaskFileModel> GetFilesByTaskInstanceIdAndFileType(Guid taskInstanceId, FileType fileType)
        {
            return taskFileRepository.GetTaskFileByTaskInstanceAndFileTypeId(taskInstanceId, fileType).ToList();
        }

        public IList<SharepointAttachmentsModel> GetTaskFileForSharepoint(Guid taskInstanceId, string fileType)
        {
            return taskFileRepository.GetTaskFileForSharepoint(taskInstanceId, fileType).ToList();
        }


        public void UpdateTaskStatus(Guid taskInstanceId)
        {
            taskRepository.UpdateTaskStatus(taskInstanceId);
        }
        public string GetAssignedCloserByFHANumber(string FHANumber)
        {
            return taskRepository.GetAssignedCloserByFHANumber(FHANumber);
        }

        public bool IsFirmCommitmentExists(Guid taskInstanceId)
        {
            return taskRepository.IsFirmCommitmentExists(taskInstanceId);
        }

        public bool IsFirmCommitmentCompleted(Guid taskInstanceId)
        {
            return taskRepository.IsFirmCommitmentCompleted(taskInstanceId);
        }
		public string GetFHANumberByTaskInstanceId(Guid taskInstanceId)
        {
            return taskRepository.GetFHANumberByTaskInstanceId(taskInstanceId);
        }


		
		/// <summary>
		/// Get tasks by fha number
		/// </summary>
		/// <param name="taskInstanceId"></param>
		/// <returns></returns>
		public IEnumerable<TaskModel> GetTasksByFHANumber(string pFHANumber)
		{
			var results = taskRepository.GetTasksByFHANumber(pFHANumber).ToList();
			return results;
		}

	}
}
