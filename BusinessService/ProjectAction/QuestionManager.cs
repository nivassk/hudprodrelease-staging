﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces.ProjectAction;
using Repository.ProjectAction;

namespace BusinessService.ProjectAction
{
    public class QuestionManager :IQuestionManager
    {
        private UnitOfWork _unitOfWorkIntermediate;
        private UnitOfWork _unitOfWorkLive;
        private IQuestionRepository questionRepository;

        public QuestionManager()
        {
            _unitOfWorkIntermediate = new UnitOfWork(DBSource.Intermediate);
            _unitOfWorkLive = new UnitOfWork(DBSource.Live);
            questionRepository = new QuestionRepository(_unitOfWorkLive);
        }

        public void AddQuestion(QuestionViewModel questionViewModel)
        {
            questionRepository.AddQuestion(questionViewModel);
            _unitOfWorkLive.Save();
        }

        public IList<QuestionViewModel> GetAllQuestions(int projectActionTypeId)
        {
           return questionRepository.GetAllQuestions(projectActionTypeId);
        }

        public void UpdateQuestion(QuestionViewModel questionViewModel)
        {
            questionRepository.UpdateQuestion(questionViewModel);
            _unitOfWorkLive.Save();
        }

        public QuestionViewModel GetQuestion(Guid checkListId)
        {
            return questionRepository.GetQuestion(checkListId);
        }
    }
}
