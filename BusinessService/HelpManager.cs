﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService
{
    public static class HelpManager
    {
        public static string RemoveZeros(string addString)
        {
            if (addString == null || addString == "0" || addString == "00") return string.Empty;
            return addString;
        } 
    }
}
