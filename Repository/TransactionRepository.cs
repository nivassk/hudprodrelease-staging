﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Runtime.ConstrainedExecution;
using System.Security.Policy;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using System.Linq;
using HUDHealthcarePortal.Model;
using Model;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class TransactionRepository:BaseRepository<Transaction>, ITransactionRepository

    {
          public TransactionRepository(): base(new UnitOfWork(DBSource.Live))
        {
        }
          public TransactionRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<Transaction> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public Guid  AddTransaction(TransactionModel transactionModel)
        {
            var trasansaction = Mapper.Map<Transaction>(transactionModel);
            if (trasansaction != null)
            {
                this.InsertNew(trasansaction);
            }
            return trasansaction.TransactionId;
        }
    }
}