﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class FhaRepository : BaseRepository<usp_HCP_GetFhasByLenderIds_Result>, IFhaRepository
    {
        public FhaRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public FhaRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<FhaInfoModel> GetFhasByLenderIds(string lenderIds)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetFhasByLenderIds, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetFhasByLenderIds_Result>("usp_HCP_GetFhasByLenderIds", 
                new {LenderIds = lenderIds}).OrderBy(p => p.FHANumber).ToList();
            return Mapper.Map<IEnumerable<usp_HCP_GetFhasByLenderIds_Result>, IEnumerable<FhaInfoModel>>(results);
        }
        public IEnumerable<FhaInfoModel> GetReadyFhasForInspectionContracotr()
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetReadyFhasForInspectionContractor, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetFhasByLenderIds_Result>("usp_HCP_Prod_GetReadyFhasForInspectionContractor").OrderBy(p => p.FHANumber).ToList();
            return Mapper.Map<IEnumerable<usp_HCP_GetFhasByLenderIds_Result>, IEnumerable<FhaInfoModel>>(results);
        }

        public IEnumerable<FhaInfoModel> GetFhasByWLMIds(int wlmid)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetFhasByLenderIds, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetFhasByLenderIds_Result>("usp_HCP_GetFhasByWLMId",
                new { wlmid = wlmid }).OrderBy(p => p.FHANumber).ToList();
            return Mapper.Map<IEnumerable<usp_HCP_GetFhasByLenderIds_Result>, IEnumerable<FhaInfoModel>>(results);
        }
    }
}
