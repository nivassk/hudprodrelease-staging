﻿using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;
using Repository;
using Repository.Interfaces;
using HUDHealthcarePortal.Model.AssetManagement;
using Model;

namespace HUDHealthcarePortal.Repository
{
    public class NonCriticalRepairsRepository : BaseRepository<NonCriticalRepairsRequest>, INonCriticalRepairsRepository
    {
        public NonCriticalRepairsRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<NonCriticalRepairsRequest> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        /// <summary>
        /// insert non critical request on submit
        /// </summary>
        /// <param name="model"></param>
        /// <param name="nonCriticalReferReasons"></param>
        /// <returns></returns>
        public Guid SaveNonCriticalRepairsRequest(NonCriticalRepairsViewModel model, List<NonCriticalReferReasonModel> nonCriticalReferReasons)
        {
            var context = (HCP_live) this.Context;
            var nonCriticalRepairsRequest = Mapper.Map<NonCriticalRepairsRequest>(model);
            if (nonCriticalReferReasons != null && nonCriticalReferReasons.Any())
            {
                foreach (var reason in nonCriticalReferReasons)
                {
                    nonCriticalRepairsRequest.NonCriticalReferReasons.Add(Mapper.Map<NonCriticalReferReason>(reason));
                }
            }
            this.InsertNew(nonCriticalRepairsRequest);
            return nonCriticalRepairsRequest.NonCriticalRepairsRequestID;
        }


        /// <summary>
        /// updated the existing Non critical request
        /// </summary>
        /// <param name="model"></param>
        public void UpdateNonCriticalRepairsRequest(NonCriticalRepairsViewModel model,
            INonCriticalProjectInfoRepository nonCriticalProjectInfoRepository,
            ITransactionRepository transactionRepository)
        {
            var context = (HCP_live) this.Context;
            var nonCriticalRequest = (from n in context.NonCriticalRepairsRequest
                where n.NonCriticalRepairsRequestID == model.NonCriticalRequestId
                select n).FirstOrDefault();

            if (nonCriticalRequest != null)
            {
                nonCriticalRequest.ModifiedBy = model.ModifiedBy;
                nonCriticalRequest.ModifiedOn = model.ModifiedOn;
                nonCriticalRequest.ApprovedDate = model.ModifiedOn;
                nonCriticalRequest.RequestStatus = model.Decision;

                if (model.RequestStatus == (int) RequestStatus.Deny)
                {
                    nonCriticalRequest.NumberDraw = nonCriticalRequest.NumberDraw - 1;
                }

                if (model.RequestStatus == (int) RequestStatus.Approve ||
                    model.RequestStatus == (int) RequestStatus.ApproveWithChanges)
                {
                    var transactionModel = Mapper.Map<TransactionModel>(model);

                    nonCriticalRequest.ApprovedAmount = model.RequestStatus == (int) RequestStatus.Approve
                        ? model.PaymentAmountRequested
                        : model.ApprovedAmount;

                    transactionModel.Amount = nonCriticalRequest.ApprovedAmount;
                    transactionModel.CurrentBalance = model.NCRECurrentBalance;
                    transactionModel.TransactionId = Guid.NewGuid();
                    transactionModel.ModifiedBy = model.ModifiedBy;
                    transactionModel.ModifiedOn = DateTime.UtcNow;
                    transactionModel.CreatedBy = model.ModifiedBy;
                    transactionModel.CreatedOn = DateTime.UtcNow;
                    transactionModel.TransactionType = (int) TransactionType.NCRReimbursement;
                    transactionModel.FormType = (int) FormType.Ncr;

                    transactionRepository.AddTransaction(transactionModel);

                    var noncriticalProjectInfo =
                        nonCriticalProjectInfoRepository.GetNonCriticalProjectInfoByNCR(nonCriticalRequest);

                    noncriticalProjectInfo.CurrentBalance = (model.RequestStatus == (int) RequestStatus.Approve)
                        ? (noncriticalProjectInfo.CurrentBalance - model.PaymentAmountRequested)
                        : (noncriticalProjectInfo.CurrentBalance - model.ApprovedAmount);

                    noncriticalProjectInfo.ModifiedOn = DateTime.Now;
                    noncriticalProjectInfo.ModifiedBy = model.ModifiedBy;
                }
            }
        }

        /// <summary>
        /// Updated the lastest TaskId, this would be useful to find the latest task details
        /// </summary>
        /// <param name="model"></param>
        public void  UpdateTaskId(NonCriticalRepairsViewModel model)
        {
            var context = (HCP_live)this.Context;

            var nonCriticalRequest = (from n in context.NonCriticalRepairsRequest
                                      where n.NonCriticalRepairsRequestID == model.NonCriticalRequestId
                                      select n).FirstOrDefault();
            
            nonCriticalRequest.TaskId  = model.TaskId;
            this.Update(nonCriticalRequest); 
        }

        public NonCriticalRepairsViewModel GetNCRFormById(Guid ncrId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetNCRFormById, please pass in correct context in unit of work.");

            var result = (from n in context.NonCriticalRepairsRequest
                          where n.NonCriticalRepairsRequestID == ncrId
                          select n).FirstOrDefault();

            return Mapper.Map<NonCriticalRepairsRequest, NonCriticalRepairsViewModel>(result);
        }

        public NonCriticalPropertyModel GetNonCriticalStatusAndDrawInfo(NonCriticalPropertyModel model,string fhanumber)
        {
            var context = (HCP_live) this.Context;
            var resultOne = (from n in context.NonCriticalRepairsRequest
                where n.FHANumber == fhanumber && n.RequestStatus == 1
                select n).FirstOrDefault();
            if (resultOne != null)
            {
                model.IsNCRRequestPending = true;
            }
            var resultTwo = (from n in context.NonCriticalRepairsRequest
                where n.FHANumber == fhanumber
                select n).OrderByDescending(p => p.NumberDraw).FirstOrDefault();
            model.DrawNumber = resultTwo != null ? resultTwo.NumberDraw : 0;
            model.DrawNumber += 1;


            model.IsTransactionLedger = GetTransactionLedgerStatus(fhanumber);
            return model;
        }

        public bool GetTransactionLedgerStatus(string fhanumber)
        {
            var context = (HCP_live)this.Context;
            var resultThree = (from n in context.NonCriticalRepairsRequest
                where n.FHANumber == fhanumber && (n.RequestStatus == 2 || n.RequestStatus == 3 || n.RequestStatus == 4)
                select n).FirstOrDefault();

            return resultThree!= null;
        }

        public IList<NonCriticalRulesChecklistModel> GetNonCrticalRulesCheckList(FormType formType)
        {
            var context = (HCP_live)this.Context;
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetNonCrticalRulesCheckList, please pass in correct context in unit of work.");

             var result = (from rule in context.NonCriticalRulesChecklist
                select rule).ToList();

             return Mapper.Map<List<NonCriticalRulesChecklist>, List<NonCriticalRulesChecklistModel>>(result);

        }

        public IList<NonCriticalReferReasonModel> GetNonCrticalReferReasons(Guid ncrId)
        {
            var context = (HCP_live)this.Context;

            if (context == null)
                throw new InvalidCastException("context is not from db live in GetNonCrticalReferReasons, please pass in correct context in unit of work.");


            var result = (from rule in context.NonCriticalReferReason
                          where rule.NonCriticalRepairsRequestID == ncrId
                          select rule);

             return Mapper.Map<List<NonCriticalReferReason>, List<NonCriticalReferReasonModel>>(result.ToList());
        }


        public NonCriticalPropertyModel GetNonCrticalNonCrticalTransactions(int propertyId, int lenderId, string fhaNumber, INonCriticalProjectInfoRepository nonCriticalProjectInfoRepository)
        {
            var context = (HCP_live)this.Context;
         
            if (context == null)
                throw new InvalidCastException("context is not from db live in GetNonCrticalNonCrticalTransactions, please pass in correct context in unit of work.");

            var result = (from trans in context.Transaction 
                          join ncr in context.NonCriticalRepairsRequest on trans.FormId equals ncr.NonCriticalRepairsRequestID 
                          where  ncr.PropertyID == propertyId && ncr.LenderID == lenderId && ncr.FHANumber == fhaNumber 
                          select trans).OrderBy(t=>t.CreatedOn) ;

            var npInfo = (from np in nonCriticalProjectInfoRepository.DataToJoin
                where np.PropertyID == propertyId && np.LenderID.Value == lenderId && np.FHANumber == fhaNumber
                select np).FirstOrDefault();

            var finalReuest = (from np in context.NonCriticalRepairsRequest
                               where np.PropertyID == propertyId && np.LenderID == lenderId && np.FHANumber == fhaNumber
                                     && np.IsFinalDraw == true
                         select np).FirstOrDefault();
            
            var results =  Mapper.Map<List<Transaction>, List<TransactionModel>>(result.ToList());
              return new NonCriticalPropertyModel()
             {
                IsFinalRequest = finalReuest != null ? true:false,
                FinalRquestId = finalReuest != null ? finalReuest.NonCriticalRepairsRequestID : Guid.Empty,
                NonCriticalAccountBalance = npInfo != null ? npInfo.InitialNCREBalance : 0,
                PropertyId  = propertyId,
                Transactions  = results
            };
        }
    }
}
