﻿using System.Collections;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;
using Model;

namespace Repository.Interfaces
{
    public interface INonCriticalProjectInfoRepository : IJoinable<NonCritical_ProjectInfo>
    {
        List<NonCriticalLateAlertsModel> GetNonCriticalLateAlertsToSend(string date);
		NonCriticalPropertyModel GetNonCriticalPropertyInfo(string fhaNumber);
        NonCritical_ProjectInfo GetNonCriticalProjectInfoByNCR(NonCriticalRepairsRequest nonCriticalRepairsRequest);
        NonCritical_ProjectInfo GetNonCriticalProjectInfo(int propertyId, int lenderId, string fhaNumber);
        void UpdateNonCriticalProjectInfo(NonCritical_ProjectInfo nonCriticalProjectInfo);
    }
}
