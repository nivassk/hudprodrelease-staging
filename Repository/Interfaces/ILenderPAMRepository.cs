﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;

// User story 1904
namespace Repository.Interfaces
{
    public interface ILenderPAMRepository
    {
        PAMReportModel GetLenderPAMReportSummary( string lamIds, string bamIds, string larIds, string roleIds, DateTime? fromDate, DateTime? toDate,  int? status, string projectAction, int page, string sort, SqlOrderByDirecton sortdir);

        IEnumerable<UserInfoModel> GetLenderUsersByRoleLenderId(int lenderId,string roleName);

        PAMReportModel GetSearchCriteriaDetails(ref PAMReportModel reportModel, string lamIds, string bamIds, string larIds, string roleIds, int? status,
          string projectAction, DateTime? fromDate, DateTime? toDate, string userRole, int lenderId);

        IEnumerable<KeyValuePair<int, string>> GetPamProjectActionTypesByIDs(string projectActionList);

        IEnumerable<KeyValuePair<int, string>> GetUserNameByIDs(string userIDList);

    }
}
