﻿using System;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using Model.Production;


namespace Repository.Interfaces
{
   public interface IHUDPamFiltersRepository
    {
        string SaveHUDPamFilters(HUDPamReportFiltersModel Model);
        HUDPamReportFiltersModel GetSavedFilter();
    }
}
