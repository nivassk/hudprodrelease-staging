﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface IReportsRepository
    {
        IEnumerable<ReportsModel> GetMultipleLoanProperty();

    }
}
