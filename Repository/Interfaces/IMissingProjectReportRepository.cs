﻿using Model;

namespace Repository.Interfaces
{
    public interface IMissingProjectReportRepository
    {
        ReportModel GetMissingProjectReportForSuperUser(int year);
        ReportModel GetMissingProjectReportForSuperUserWithWLM(string userName, string userType, int year);
        ReportModel GetMissingProjectReportForWorkloadManager(string userName, string userType, int year);
        ReportModel GetMissingProjectReportForAccountExecutive(string userName, string userType, int year);
    }
}
