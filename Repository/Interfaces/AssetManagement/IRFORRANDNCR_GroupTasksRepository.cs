﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using Model.AssetManagement;
using Repository.Interfaces;

namespace Repository.Interfaces.AssetManagement
{
    public interface IRFORRANDNCR_GroupTasksRepository
    {
        RFORRANDNCR_GroupTaskModel GetGroupTaskAByTaskInstanceId(Guid taskInstanceId);
        Guid AddR4RGroupTasks(RFORRANDNCR_GroupTaskModel model);
        void DeleteGroupTask(Guid TaskInstanceId, string FHANumber);

    }
}
