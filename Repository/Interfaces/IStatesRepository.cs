﻿using System.Collections.Generic;

namespace Repository.Interfaces
{
    public interface IStatesRepository
    {
        System.Collections.Generic.List<KeyValuePair<string, string>> GetAllStates();
    }
}
