﻿using AutoMapper;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDB = EntityObject.Entities.HCP_task;
namespace HUDHealthcarePortal.Repository
{
    public class RequestAdditionalInfoFilesRepository : BaseRepository<RequestAdditionalInfoFiles>,IRequestAdditionalInfoFilesRepository
    {
        public RequestAdditionalInfoFilesRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }

        public RequestAdditionalInfoFilesRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public void SaveRequestAdditionalInfoFiles(IList<RequestAdditionalInfoFileModel> fileList)
        {
            var context = (TaskDB.HCP_task)this.Context;

            if (fileList != null && fileList.Any())
            {
                foreach (var item in fileList)
                {
                    var result = Mapper.Map<RequestAdditionalInfoFiles>(item);
                    this.InsertNew(result);
                }
            }
            
        }
    }
}
