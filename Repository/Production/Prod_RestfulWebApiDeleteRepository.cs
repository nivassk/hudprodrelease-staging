﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository.Interfaces;
using Model.Production;
using System.IO;
using System.Net.Http;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using Repository.Interfaces.Production;
using System.Configuration;
using System.Net.Http.Headers;

namespace Repository.Production
{
    public class Prod_RestfulWebApiDeleteRepository : IProd_RestfulWebApiDeleteRepository
    {

        public RestfulWebApiResultModel DeleteDocument(string docId, string token)
        {
            string URL = ConfigurationManager.AppSettings["DeleteDocument"].ToString() + token;
            String encoded = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                       string.Format("{0}:{1}", "restapp", "restapp")));
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(BaseURL + URL);
            request.Method = "DELETE";
            
            request.Headers.Add("Authorization", "Basic " + encoded);
            request.ContentType = "application/json";
            request.Headers.Add("Data-Type", "application/json");
            var bytes = default(byte[]);
            string json = "{\"documentId\":\""+ docId+"\"}";
            MemoryStream memstream = new MemoryStream();
            try
            {

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {

                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)request.GetResponse();


                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    streamReader.BaseStream.CopyTo(memstream);
                    bytes = memstream.ToArray();
                    var str = System.Text.Encoding.Default.GetString(bytes);
                    result = JsonConvert.DeserializeObject<RestfulWebApiResultModel>(str);

                }



            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (memstream != null)
                {

                    memstream.Close();
                }
            }
            return result;
        }
        public RestfulWebApiResultModel DeleteDocumentFolder(string JsonStr, string token)
        {
            string URL = ConfigurationManager.AppSettings["DeleteDocumentFolder"].ToString() + token;
            String encoded = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(
                                                       string.Format("{0}:{1}", "restapp", "restapp")));
            string BaseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            RestfulWebApiResultModel result = new RestfulWebApiResultModel();
            //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//3072
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(BaseURL + URL);
            request.Method = "POST";

            request.Headers.Add("Authorization", "Basic " + encoded);
            request.ContentType = "application/json";
            request.Headers.Add("Data-Type", "application/json");
            var bytes = default(byte[]);
            
            MemoryStream memstream = new MemoryStream();
            try
            {

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {

                    streamWriter.Write(JsonStr);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)request.GetResponse();


                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    streamReader.BaseStream.CopyTo(memstream);
                    bytes = memstream.ToArray();
                    var str = System.Text.Encoding.Default.GetString(bytes);
                    result = JsonConvert.DeserializeObject<RestfulWebApiResultModel>(str);

                }



            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (memstream != null)
                {

                    memstream.Close();
                }
            }
            return result;
        }
    }
}
