﻿using HUDHealthcarePortal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;

namespace Repository
{
	public class ExhibitLetterTypeRepository : BaseRepository<ExhibitLetterType>, IExhibitLetterTypeRepository
	{
		public ExhibitLetterTypeRepository()
			: base(new UnitOfWork(DBSource.Task))
		{
		}
		public ExhibitLetterTypeRepository(UnitOfWork unitOfWork)
			: base(unitOfWork)
		{
		}

		public IQueryable<ExhibitLetterType> DataToJoin
		{
			get { return DbQueryRoot; }
		}

		public List<ExhibitLetterTypeModel> GetExhibitLetterTypes()
		{
			var exhibitLetterType = this.GetAll().ToList();
			return Mapper.Map<List<ExhibitLetterTypeModel>>(exhibitLetterType);
		}
		public string GetExhibitLetterTypeName(int pId)
		{
			ExhibitLetterType objExhibitLetterType = this.GetByID(pId);
			if (objExhibitLetterType != null)
				return objExhibitLetterType.ExhibitLetter;
			return null;
		}

	}
}



