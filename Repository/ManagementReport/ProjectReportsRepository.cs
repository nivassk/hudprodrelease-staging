﻿using System;
using System.Linq;
using Core;
using Core.Utilities;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using AutoMapper;
using Model;

namespace Repository.ManagementReport
{
    public class ProjectReportsRepository : BaseRepository<ProjectReportsViewModel>, IProjectReportsRepository
    {
        private ProjectReportModel _projectReportModel;
        private ILenderFhaRepository _lenderFhaRepository;
        private IProjectInfoRepository _projectInfoRepository;
        const int TrueInt = 1;
        const int FalseInt = 0;


        public ProjectReportsRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
            _lenderFhaRepository = new LenderFhaRepository();
            _projectInfoRepository = new ProjectInfoRepository();
            _projectReportModel = new ProjectReportModel(ReportType.ProjectReport);
        }

        public ProjectReportsRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public ProjectReportModel GetProjectReportSummary(ReportLevel reportLevel, HUDRole hudRole, string userName,
            string wlmId, string aeId, string lenderId, ReportType reportType, DateTime minUploadDate,
            DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio, bool isWorkingCapital,
            bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            var context = this.Context as HCP_live;

            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetProjectReportSummary, please pass in correct context in unit of work.");

            var results = context.Database.SqlQuerySimple<ProjectReportsViewModel>("usp_HCP_Get_Project_Summary",
            new
            {
                ReportLevel = reportLevel.ToString("g"),
                UserName = userName,
                UserType = hudRole.ToString("g"),
                ReportType = reportType.ToString("g"),
                WlmId = wlmId,
                AeId = aeId,
                LenderId = lenderId,
                MinUploadDate = minUploadDate,
                MaxUploadDate = maxUploadDate,
                IsHighLoan = isHighLoan,
                IsDebtCoverageRatio = isDebtCoverageRatio,
                IsWorkingCapital = isWorkingCapital,
                IsDaysCashOnHand = isDaysCashOnHand,
                IsDaysInAcctReceivable = isDaysInAcctReceivable,
                IsAvgPaymentPeriod = isAvgPaymentPeriod
            }).ToList();

            HUDWorkLoadManagerRepository wlmRepository = new HUDWorkLoadManagerRepository();
            HUDAccountExecutiveRepository aeRepository = new HUDAccountExecutiveRepository();
            LenderInfoRepository lenderRepository = new LenderInfoRepository();

            var reportModel = new ProjectReportModel(reportType);
            reportModel.ProjectReportGridlList = results.ToList();
            int wlmIdInt, aeIdInt, lenderIdInt;

            if (!String.IsNullOrEmpty(wlmId) && int.TryParse(wlmId, out wlmIdInt))
            {
                reportModel.ReportHeaderModel.HudWorkloadManagerId = wlmIdInt;
                if (wlmIdInt == -1)
                {
                    reportModel.ReportHeaderModel.HudWorkloadManagerName = "Not Assigned";
                }
                else
                {
                    reportModel.ReportHeaderModel.HudWorkloadManagerName =
                        wlmRepository.GetWorkloadMgrById(wlmIdInt).HUD_WorkLoad_Manager_Name;
                }

            }
            if (!String.IsNullOrEmpty(aeId) && int.TryParse(aeId, out aeIdInt))
            {
                reportModel.ReportHeaderModel.HudProjectManagerId = aeIdInt;
                if (aeIdInt == -1)
                {
                    reportModel.ReportHeaderModel.HudProjectManagerName = "Not Assigned";
                }
                else
                {
                    reportModel.ReportHeaderModel.HudProjectManagerName =
                        aeRepository.GetProjectManagerById(aeIdInt).HUD_Project_Manager_Name;
                }
            }

            return reportModel;
        }

        public ProjectReportModel GetProjectReportDetail(string wlmId, string aeId, string lenderId, ReportType reportType, DateTime minUploadDate, DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetProjectReportDetail, please pass in correct context in unit of work.");

            var results = context.Database.SqlQuerySimple<ProjectReportsViewModel>("usp_HCP_Get_Project_Detail",
                new
                {
                    ReportType = reportType.ToString("g"),
                    WlmId = wlmId,
                    AeId = aeId,
                    LenderId = lenderId,
                    MinUploadDate = minUploadDate,
                    MaxUploadDate = maxUploadDate,
                    IsHighLoan = isHighLoan,
                    IsDebtCoverageRatio = isDebtCoverageRatio,
                    IsWorkingCapital = isWorkingCapital,
                    IsDaysCashOnHand = isDaysCashOnHand,
                    IsDaysInAcctReceivable = isDaysInAcctReceivable,
                    IsAvgPaymentPeriod = isAvgPaymentPeriod
                }).ToList();

            var reportModel = new ProjectReportModel(reportType);
            reportModel.ProjectReportGridlList = results.ToList();

            HUDWorkLoadManagerRepository wlmRepository = new HUDWorkLoadManagerRepository();
            HUDAccountExecutiveRepository aeRepository = new HUDAccountExecutiveRepository();
            LenderInfoRepository lenderRepository = new LenderInfoRepository();

            int wlmIdInt, aeIdInt, lenderIdInt;
            if (!String.IsNullOrEmpty(wlmId) && int.TryParse(wlmId, out wlmIdInt))
            {
                reportModel.ReportHeaderModel.HudWorkloadManagerId = wlmIdInt;
                if (wlmIdInt == -1)
                {
                    reportModel.ReportHeaderModel.HudWorkloadManagerName = "Not Assigned";
                }
                else
                {
                    reportModel.ReportHeaderModel.HudWorkloadManagerName =
                        wlmRepository.GetWorkloadMgrById(wlmIdInt).HUD_WorkLoad_Manager_Name;
                }

            }
            if (!String.IsNullOrEmpty(aeId) && int.TryParse(aeId, out aeIdInt))
            {
                reportModel.ReportHeaderModel.HudProjectManagerId = aeIdInt;
                if (aeIdInt == -1)
                {
                    reportModel.ReportHeaderModel.HudProjectManagerName = "Not Assigned";
                }
                else
                {
                    reportModel.ReportHeaderModel.HudProjectManagerName =
                        aeRepository.GetProjectManagerById(aeIdInt).HUD_Project_Manager_Name;
                }
            }
            if (!String.IsNullOrEmpty(lenderId) && int.TryParse(lenderId, out lenderIdInt))
            {
                reportModel.ReportHeaderModel.LenderId = lenderIdInt;
                if (lenderIdInt == -1)
                {
                    reportModel.ReportHeaderModel.LenderName = "Not Assigned";
                }
                else
                {
                    var lender = lenderRepository.GetLenderDetail(lenderIdInt);
                    if (lender != null)
                    {
                        reportModel.ReportHeaderModel.LenderName = lender.FirstOrDefault().Value;
                    }
                }
            }
            return reportModel;
        }

        public string GetWlmId(string userName)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetProjectReportDetail, please pass in correct context in unit of work.");

            var result = context.Database.SqlQuerySimple<usp_HCP_Get_WLM_AE_Details_Result>(
                "usp_HCP_Get_WLM_AE_Details",
                new { Username = userName });
            ReportHeaderModel reportHeaderModel = Mapper.Map<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>(result.First());
            return (reportHeaderModel.HudWorkloadManagerId.ToString());
        }

        public string GetAeId(string userName)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db Live in GetProjectReportDetail, please pass in correct context in unit of work.");

            var result = context.Database.SqlQuerySimple<usp_HCP_Get_WLM_AE_Details_Result>(
                "usp_HCP_Get_WLM_AE_Details",
                new { Username = userName });
            ReportHeaderModel reportHeaderModel = Mapper.Map<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>(result.First());
            return (reportHeaderModel.HudProjectManagerId.ToString());
        }

    }
}
