﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using AutoMapper;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class LenderFiscalYearDetailsRepository : BaseRepository<LenderFiscalYearDetails>, ILenderFiscalYearDetailsRepository
    {
        public LenderFiscalYearDetailsRepository(): base(new UnitOfWork(DBSource.Live)){}

        public LenderFiscalYearDetailsRepository(UnitOfWork unitOfWork): base(unitOfWork){}

        public IQueryable<LenderFiscalYearDetails> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public int? GetFiscalYearEndingMonth(int lenderId, string fha)
        {
            var context = this.Context as HCP_live;
            var result = (from l in context.LenderFiscalYearDetails
                          where l.LenderId == lenderId && l.FHANumber == fha
                          orderby l.Id descending
                          select l.FiscalYearEndMonth
                          ).FirstOrDefault();
            return result;
        }
    }
}
