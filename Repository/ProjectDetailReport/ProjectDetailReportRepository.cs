﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model;
using Repository.Interfaces;


namespace Repository.ProjectDetailReport
{
   public  class ProjectDetailReportRepository : BaseRepository<Lender_DataUpload_Live>, IProjectDetailReportRepository
    {

       private ReportModel _missingProjectReportModel;
        private ILenderFhaRepository _lenderFhaRepository;
        private IProjectInfoRepository _projectInfoRepository;

        public ProjectDetailReportRepository()

            : base(new UnitOfWork(DBSource.Live))
        {
            _lenderFhaRepository = new LenderFhaRepository();
            _projectInfoRepository = new ProjectInfoRepository();
            _missingProjectReportModel = new ReportModel(ReportType.MissingProjectReport);
        }


        public ReportModel GetProjectDetailReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_Project_Detail_ManagementReport, please pass in correct context in unit of work.");

            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetProjectDetailReport_Result>(
                    "usp_HCP_GetProjectDetailReport",
                    new { UserType = userType, Username = userName, @QueryDate = QryDate }).ToList();
            var reportModel = new ReportModel(ReportType.ProjectDetailReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
               Mapper.Map<IEnumerable<usp_HCP_GetProjectDetailReport_Result>, IEnumerable<ReportGridModel>>(
             
                   results).ToList();

            
            if (reportModel.ReportGridModelList.Count > 0)
            {
             
                var ProjectCnt = reportModel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();
                var ErrorCnt = (from od in reportModel.ReportGridModelList where od.IsProjectError == 1 select od.FhaNumber).Distinct().Count();
                
                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError==1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();
                var reportHeaderModel =
                    Mapper.Map<ReportGridModel, ReportHeaderModel>(reportModel.ReportGridModelList[0]);
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
                if (HUDRole.LenderAccountRepresentative.ToString().ToUpper() == userType.ToUpper() || HUDRole.LenderAccountRepresentative.ToString().ToUpper() == userType.ToUpper() || HUDRole.BackupAccountManager.ToString().ToUpper() == userType.ToUpper() || HUDRole.InternalSpecialOptionUser.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectDetailReport;
                }
                else
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectDetailReport;
                }

                //Adding the Extra row for the Error Count
                //var ErrorModel = new ReportGridModel();
                //ErrorModel.QuaterlyNoi = TotalNoiError.ToString();
                //ErrorModel.QuarterlyDSCR = TotalDSCRError.ToString();
                //ErrorModel.AverageDailyRateRatio=Convert.ToDecimal(TotalADRError);
                //ErrorModel.RowWiseErrorchar = TotalColumErrors.ToString();
                //ErrorModel.PeriodEnding = "Ingnore";
           

                //reportModel.ReportGridModelList.Insert(0, ErrorModel);
                //End  Logic for adding  Error Count

                reportModel.ReportHeaderModel.TotalProperties = ProjectCnt;
                reportModel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
                reportModel.ReportHeaderModel.TotalDSCRError = TotalDSCRError;
                reportModel.ReportHeaderModel.TotalNoiError = TotalNoiError;
                reportModel.ReportHeaderModel.TotalOpRevError = TotalOpRevError;
                reportModel.ReportHeaderModel.TotalADRError = TotalADRError;
                reportModel.ReportHeaderModel.TotalColError = TotalColumErrors;

            }
            else
            {
                var reportHeaderModel = new ReportHeaderModel();
                //var result = context.Database.SqlQuerySimple<usp_HCP_Get_WLM_AE_Details_Result>("usp_HCP_Get_WLM_AE_Details",
                //    new { Username = userName });
                //reportHeaderModel = Mapper.Map<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>(result.First());

                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.ReportType = ReportType.ProjectDetailReport;
                reportModel.ReportHeaderModel.TotalProperties = 0;
                reportModel.ReportHeaderModel.TotalPropertieswithException = 0;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
                reportModel.ReportHeaderModel.TotalDSCRError = 0;
                reportModel.ReportHeaderModel.TotalNoiError = 0;
                reportModel.ReportHeaderModel.TotalOpRevError = 0;
                reportModel.ReportHeaderModel.TotalADRError = 0;
                reportModel.ReportHeaderModel.TotalColError = 0;
            }
            return reportModel;
        }

        public ReportModel GetProjectDetailReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016")
        {
            return GetProjectDetailReportForSuperUser(userName, userType, QryDate);
        }

        public ReportModel GetProjectDetailReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            return GetProjectDetailReportForSuperUser(userName, userType, QryDate);
        }


        public ReportModel GetProjectDetailReportForSuperUserHighLevel(string userName, string userType, string SortOrder = "ASC", string QryDate = "4/21/2016")
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_Project_Detail_ManagementReport_HighLevel, please pass in correct context in unit of work.");

            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetProjectDetailReport_HighLevel_Result>(
                    "usp_HCP_GetProjectDetailReport_HighLevel",
                    new { UserType = userType, Username = userName, SortOrder = SortOrder, QueryDate = QryDate }).ToList();
     
            var reportModel = new ReportModel(ReportType.ProjectDetailReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
               Mapper.Map<IEnumerable<usp_HCP_GetProjectDetailReport_HighLevel_Result>, IEnumerable<ReportGridModel>>(
                   results).ToList();
            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ProjectCnt = (from od in reportModel.ReportGridModelList where od.isWLM==1 select od.ProjectCountWLM).Sum();
                var ErrorCnt = (from od in reportModel.ReportGridModelList where od.isWLM==1 select od.ErrorCountWLM ).Sum();
                var Aggregate = new ReportGridModel();
                //Aggregate.WLMName = "<b>Total</b>";
                //Aggregate.ProjectCount = ProjectCnt;
                //Aggregate.ErrorCount = ErrorCnt;
                //reportModel.ReportGridModelList.Add(Aggregate);
                var reportHeaderModel =
                    Mapper.Map<ReportGridModel, ReportHeaderModel>(reportModel.ReportGridModelList[0]);
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
                reportModel.ReportHeaderModel.ReportType = ReportType.ProjectDetailReport;
                reportModel.ReportHeaderModel.TotalProperties = ProjectCnt;
                reportModel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;

            }
            else
            {
                var reportHeaderModel = new ReportHeaderModel();
                //var result = context.Database.SqlQuerySimple<usp_HCP_Get_WLM_AE_Details_Result>("usp_HCP_Get_WLM_AE_Details",
                //    new { Username = userName });
                //reportHeaderModel = Mapper.Map<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>(result.First());
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.ReportType = ReportType.ProjectDetailReport;
              
                reportModel.ReportHeaderModel.TotalProperties = 0;
                reportModel.ReportHeaderModel.TotalPropertieswithException = 0;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
            }
            return reportModel;
        }

       //Error Reports

        public ReportModel GetErrorReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_Project_Detail_ErrorReport, please pass in correct context in unit of work.");

            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetProjectDetailReport_Result>(
                    "usp_HCP_GetProjectDetailReport",
                    new { UserType = userType, Username = userName, @QueryDate = QryDate }).ToList();
            var reportModel = new ReportModel(ReportType.ProjectDetailReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
               Mapper.Map<IEnumerable<usp_HCP_GetProjectDetailReport_Result>, IEnumerable<ReportGridModel>>(
                   results).ToList();
            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ErrorCnt = (from od in reportModel.ReportGridModelList where od.IsProjectError == 1 select od.FhaNumber).Distinct().Count();
                var ProjectCnt = reportModel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();

                reportModel.ReportGridModelList = reportModel.ReportGridModelList.Where(m => m.IsProjectError == 1 && m.RowWiseError > 0).ToList();
                var TotalNoiError = (from od in reportModel.ReportGridModelList where od.IsQtrlyNOIError == 1 || od.IsQtrlyNOIPTError == 1 select od).ToList().Count();
                var TotalDSCRError = (from od in reportModel.ReportGridModelList where od.IsQtrlyDSCRError == 1 || od.IsQtrlyDSCRPTError == 1 select od).Count();
                var TotalADRError = (from od in reportModel.ReportGridModelList where od.IsADRError == 1 select od).Count();
                var TotalOpRevError = (from od in reportModel.ReportGridModelList where od.IsQtrlyORevError == 1 select od).Count();
                var TotalColumErrors = reportModel.ReportGridModelList.Select(t => t.RowWiseError ?? 0).Sum();

                var reportHeaderModel =
                    Mapper.Map<ReportGridModel, ReportHeaderModel>(reportModel.ReportGridModelList[0]);
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;

                reportModel.ReportHeaderModel.ReportType = ReportType.ProjectErrorReportAE;
                if (HUDRole.AccountExecutive.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectErrorReportAE;
                }

                else if (HUDRole.WorkflowManager.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectErrorReportWLM;
                }

                else if (HUDRole.SuperUser.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectErrorReportAdmin;
                }
        
                reportModel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
                reportModel.ReportHeaderModel.TotalDSCRError = TotalDSCRError;
                reportModel.ReportHeaderModel.TotalNoiError = TotalNoiError;
                reportModel.ReportHeaderModel.TotalOpRevError = TotalOpRevError;
                reportModel.ReportHeaderModel.TotalADRError = TotalADRError;
                reportModel.ReportHeaderModel.TotalColError = TotalColumErrors;

            }
            else
            {
                var reportHeaderModel = new ReportHeaderModel();
                reportModel.ReportHeaderModel = reportHeaderModel;
                //var result = context.Database.SqlQuerySimple<usp_HCP_Get_WLM_AE_Details_Result>("usp_HCP_Get_WLM_AE_Details",
                //    new { Username = userName });
                //reportHeaderModel = Mapper.Map<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>(result.First());
                reportModel.ReportHeaderModel.ReportType = ReportType.ProjectErrorReportAE;
               
                reportModel.ReportHeaderModel.TotalProperties = 0;
                reportModel.ReportHeaderModel.TotalPropertieswithException = 0;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
                reportModel.ReportHeaderModel.TotalDSCRError = 0;
                reportModel.ReportHeaderModel.TotalNoiError = 0;
                reportModel.ReportHeaderModel.TotalOpRevError = 0;
                reportModel.ReportHeaderModel.TotalADRError = 0;
                reportModel.ReportHeaderModel.TotalColError = 0;
            }
            return reportModel;
        }

        public ReportModel GetErrorReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016")
        {
            return GetErrorReportForSuperUser(userName, userType, QryDate);
        }

        public ReportModel GetErrorReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            return GetErrorReportForSuperUser(userName, userType, QryDate);
        }




        public ReportModel GetPTReportForSuperUser(string userName, string userType, string QryDate = "4/21/2016")
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in Get_PT_ManagementReport, please pass in correct context in unit of work.");

            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetProjectDetailReport_PT_Result>(
                    "usp_HCP_GetProjectDetailReport_PT",
                    new { UserType = userType, Username = userName, @QueryDate = QryDate }).ToList();
            var reportModel = new ReportModel(ReportType.ProjectDetailReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
               Mapper.Map<IEnumerable<usp_HCP_GetProjectDetailReport_PT_Result>, IEnumerable<ReportGridModel>>(
                   results).ToList();
            var FilteredList = reportModel.ReportGridModelList.Where(x => x.IsQtrlyDSCRPTError==1 ||x.IsQtrlyNOIPTError==1 ).Distinct().ToList();
            reportModel.ReportGridModelList = null;
            reportModel.ReportGridModelList = FilteredList;

            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ProjectCnt = reportModel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();
                var ErrorCnt = (from od in reportModel.ReportGridModelList where od.IsProjectError == 1 select od.FhaNumber).Distinct().Count();
                var reportHeaderModel =
                    Mapper.Map<ReportGridModel, ReportHeaderModel>(reportModel.ReportGridModelList[0]);
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
             
                reportModel.ReportHeaderModel.TotalProperties = ProjectCnt;
                reportModel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
                reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReportAE;

                if (HUDRole.AccountExecutive.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReportAE;
                }

                else if (HUDRole.WorkflowManager.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReportWLM;
                }

                else if (HUDRole.SuperUser.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReport;
                }

            }
            else
            {
                var reportHeaderModel = new ReportHeaderModel();
                reportModel.ReportHeaderModel = reportHeaderModel;
                //var result = context.Database.SqlQuerySimple<usp_HCP_Get_WLM_AE_Details_Result>("usp_HCP_Get_WLM_AE_Details",
                //    new { Username = userName });
                //reportHeaderModel = Mapper.Map<usp_HCP_Get_WLM_AE_Details_Result, ReportHeaderModel>(result.First());
                reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReportAE;
                if (HUDRole.AccountExecutive.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReportAE;
                }

                else if (HUDRole.WorkflowManager.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReportWLM;
                }

                else if (HUDRole.SuperUser.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.ProjectPTReport;
                }
              
                reportModel.ReportHeaderModel.TotalProperties = 0;
                reportModel.ReportHeaderModel.TotalPropertieswithException = 0;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
            }
            return reportModel;
        }

        public ReportModel GetPTReportForWorkloadManager(string userName, string userType, string QryDate = "4/21/2016")
        {
            return GetPTReportForSuperUser(userName, userType, QryDate);
        }

        public ReportModel GetPTReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            return GetPTReportForSuperUser(userName, userType, QryDate);
        }


        public ReportModel GetCurrentQuarterReportForAccountExecutive(string userName, string userType, string QryDate = "4/21/2016")
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in AECurrentQuarterReport, please pass in correct context in unit of work.");

            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetProjectDetailReport_CurrentQuarter_Result>(
                    "usp_HCP_GetProjectDetailReport_CurrentQuarter",
                    new { UserType = userType, Username = userName, @QueryDate = QryDate }).ToList();
            var reportModel = new ReportModel(ReportType.ProjectDetailReport);
            reportModel.ReportGridModelList = new List<ReportGridModel>();
            reportModel.ReportGridModelList =
               Mapper.Map<IEnumerable<usp_HCP_GetProjectDetailReport_CurrentQuarter_Result>, IEnumerable<ReportGridModel>>(
                   results).ToList();
           
            

            if (reportModel.ReportGridModelList.Count > 0)
            {
                var ProjectCnt = reportModel.ReportGridModelList.Select(x => x.FhaNumber).Distinct().Count();
                var UnitsInFacility=reportModel.ReportGridModelList.Sum(o=>o.UnitsInFacility);
                var ErrorCnt = (from od in reportModel.ReportGridModelList where od.IsProjectError == 1 select od.FhaNumber).Distinct().Count();
                var reportHeaderModel =
                    Mapper.Map<ReportGridModel, ReportHeaderModel>(reportModel.ReportGridModelList[0]);
                reportModel.ReportHeaderModel = reportHeaderModel;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;

                reportModel.ReportHeaderModel.TotalProperties = ProjectCnt;
                reportModel.ReportHeaderModel.TotalPropertieswithException = ErrorCnt;
                reportModel.ReportHeaderModel.TotalNumberOfUnits = UnitsInFacility;
                reportModel.ReportHeaderModel.ReportType = ReportType.AECurrentQuarterReport;

                if (HUDRole.AccountExecutive.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.AECurrentQuarterReport;
                }


                else if (HUDRole.WorkflowManager.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.WLMCurrentQuarterReport;
                }

                else if (HUDRole.SuperUser.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.AdminCurrentQuarterReport;
                }
            }
            else
            {
                var reportHeaderModel = new ReportHeaderModel();
                reportModel.ReportHeaderModel = reportHeaderModel;
                if (HUDRole.AccountExecutive.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.AECurrentQuarterReport;
                }


                else if (HUDRole.WorkflowManager.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.WLMCurrentQuarterReport;
                }

                else if (HUDRole.SuperUser.ToString().ToUpper() == userType.ToUpper())
                {
                    reportModel.ReportHeaderModel.ReportType = ReportType.AdminCurrentQuarterReport;
                }

                reportModel.ReportHeaderModel.ReportType = ReportType.AECurrentQuarterReport;
            
                reportModel.ReportHeaderModel.TotalProperties = 0;
                reportModel.ReportHeaderModel.TotalPropertieswithException = 0;
                reportModel.ReportHeaderModel.CreatedBy = UserPrincipal.Current.FullName;
            }
            return reportModel;
        }
    }
}
