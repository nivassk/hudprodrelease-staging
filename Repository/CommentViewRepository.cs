﻿using HUDHealthcarePortal.Core;
using System.Linq;
using EntityObject.Entities.HCP_intermediate;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class CommentViewRepository : BaseRepository<vwComment>, ICommentViewRepository
    {
        public CommentViewRepository()
            : base(new UnitOfWork(DBSource.Intermediate))
        {
        }
        public CommentViewRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<vwComment> DataToJoin
        {
            get { return DbQueryRoot; }
        }
    }
}

