﻿using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskDB = EntityObject.Entities.HCP_task;
namespace Repository
{
   public  class ReviewerTaskAssignmentRepository : BaseRepository<ReviewerTaskAssignment>, IReviewerTaskAssignmentRepository
    {


       public ReviewerTaskAssignmentRepository()
           : base(new UnitOfWork(DBSource.Task))
       {
       }

          public ReviewerTaskAssignmentRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

            
         public  int InsertReviewerTaskAssignment(ReviewerTaskAssigmentViewModel model)
          {

             var context = this.Context as TaskDB.HCP_task;
              var rTaskAssignment = Mapper.Map<ReviewerTaskAssignment>(model);
              rTaskAssignment.ModifiedOn = DateTime.UtcNow;
              this.InsertNew(rTaskAssignment);
              context.SaveChanges();
              
                return rTaskAssignment.ReviewerTaskAssignmentId;
           }

    }
}
