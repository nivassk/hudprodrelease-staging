namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LenderInfo")]
    public partial class LenderInfo
    {
        public LenderInfo()
        {
            Lender_DataUpload_Live = new HashSet<Lender_DataUpload_Live>();
            Lender_DataUpload_Live1 = new HashSet<Lender_DataUpload_Live>();
            ProjectInfoes = new HashSet<ProjectInfo>();
            User_Lender = new HashSet<User_Lender>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LenderID { get; set; }

        [Required]
        [StringLength(100)]
        public string Lender_Name { get; set; }

        public int? AddressID { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public virtual ICollection<Lender_DataUpload_Live> Lender_DataUpload_Live { get; set; }

        public virtual ICollection<Lender_DataUpload_Live> Lender_DataUpload_Live1 { get; set; }

        public virtual ICollection<ProjectInfo> ProjectInfoes { get; set; }

        public virtual ICollection<User_Lender> User_Lender { get; set; }
    }
}
