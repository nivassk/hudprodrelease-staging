﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
     [Table("HCP_User_LoginTime")]
    public partial class HCP_User_LoginTime
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid UserLoginID { get; set; }
        public DateTime Login_Time { get; set; }
        public DateTime? Logout_Time { get; set; }
        public string RoleName { get; set; }
        public string Session_Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int? User_id { get; set; }

    }
}
