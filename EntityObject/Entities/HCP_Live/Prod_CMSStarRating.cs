﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("Prod_CMSStarRating")]
    public class Prod_CMSStarRating
    {
        [Key]
        public int RatingId { get; set; }
        public string RatingName { get; set; }
    }
}
