namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PropertyID_FHANumber_iREMS
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PropertyID { get; set; }

        [Required]
        [StringLength(100)]
        public string PropertyName { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(15)]
        public string FHANumber { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public virtual FHAInfo_iREMS FHAInfo_iREMS { get; set; }

        public virtual PropertyInfo_iREMS PropertyInfo_iREMS { get; set; }
    }
}
