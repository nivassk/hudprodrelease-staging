﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    public class Prod_CongressionalDistrict
    { 
        [Key]
        public int CongressionalDTId { get; set;}
        public int ZipCode { get; set; }
        public int CongressionalDistrict { get; set; }
    }
}
