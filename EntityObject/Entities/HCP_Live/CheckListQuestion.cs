﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EntityObject.Entities.HCP_live
{
    public class CheckListQuestion
    {
            [Key]
            public Guid CheckListId { get; set; }
            public int QuestionId { get; set; }
            public string Question { get; set; }
            public int Level { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public int DisplayOrderId { get; set; }
            public int ProjectActionId { get; set; }
            public Guid ParentQuestionId { get; set; }
            public bool IsAttachmentRequired { get; set; }
            public int? CreatedBy { get; set; }
            public DateTime? CreatedOn { get; set; }
            public int? ModifiedBy { get; set; }
            public DateTime? ModifiedOn { get; set; }

            public int? SectionId { get; set; }
            public string SectionName { get; set; }
            public string Shortnames { get; set; }
            public bool IsNARequired { get; set; }
            public bool IsNAForSection { get; set; }
    }
}
