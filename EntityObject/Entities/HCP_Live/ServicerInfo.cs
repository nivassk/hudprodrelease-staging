namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ServicerInfo")]
    public partial class ServicerInfo
    {
        public ServicerInfo()
        {
            Lender_DataUpload_Live = new HashSet<Lender_DataUpload_Live>();
            User_Lender = new HashSet<User_Lender>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ServicerID { get; set; }

        [Required]
        [StringLength(100)]
        public string ServicerName { get; set; }

        public int AddressID { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? ModifiedBy { get; set; }

        public int? OnBehalfOfBy { get; set; }

        public bool? Deleted_Ind { get; set; }

        public virtual ICollection<Lender_DataUpload_Live> Lender_DataUpload_Live { get; set; }

        public virtual ICollection<User_Lender> User_Lender { get; set; }
    }
}
