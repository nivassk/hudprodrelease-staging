﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    public class usp_HCP_GetProjectDetailReport_HighLevel_Result
    {
        public string  WLMName { get; set; }
        public string Email { get; set; }
        public Nullable<int> ProjectCountWLM { get; set; }
        public Nullable<int> ErrorCountWLM { get; set; }
        public Nullable<int> ProjectCountAE { get; set; }
        public Nullable<int> ErrorCountAE { get; set; }
        public Nullable<int> isWLM { get; set; }

    }
}
