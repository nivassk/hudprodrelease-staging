﻿using System;

namespace EntityObject.Entities.HCP_live
{
    public class usp_HCP_InsertAEChecklistStatus
    {
        public Guid ProjectActionFormId { get; set; }
        public int ProjectActionTypeId { get; set;}
        public int UserId { get; set; }
    }
}