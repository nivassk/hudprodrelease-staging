﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_live
{
    [Table("InternalExternalTask")]
    public partial class InternalExternalTask
    {       
        [Key]
        public int InternalExternalTaskID { get; set; }
        public int PageTypeID { get; set; }
        public Guid TaskInstanceID { get; set; }
        public string FHANumber { get; set; }
        public int UserTypeID { get; set; }
        public int AssignedToUserID { get; set; }
        public int LenderID { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ? ModifiedOn { get; set; }
        public int ? ModifiedBy { get; set; }
        public DateTime ? ForwardedToLenderDate { get; set; }
        public DateTime ? ClosingDate { get; set; }
        public string AdditionalComments { get; set; }
        public int StatusID { get; set; }
        public DateTime ? SurveyDate { get; set; }

        public int ? InUse { get; set; }

    }
}
