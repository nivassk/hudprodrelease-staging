﻿namespace EntityObject.Entities.HCP_task
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("SaluatationType")]
	public partial class SaluatationType
	{
		[Key]
		public int SaluatationTypeId { get; set; }
		public string Saluatation { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}

