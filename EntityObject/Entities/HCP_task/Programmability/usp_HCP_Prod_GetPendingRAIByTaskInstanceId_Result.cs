﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task.Programmability
{
    public class usp_HCP_Prod_GetPendingRAIByTaskInstanceId_Result
    {
        public string userName { get; set; }
        public string userRole { get; set; }
        public string folderName { get; set; }
        public DateTime CreatedOn { get; set; }
        public string fileName { get; set; }
        public string comments { get; set; }
        public Guid taskFileId { get; set; }
        public int fileId { get; set; }
        public int downloadFileId { get; set; }
        public bool isInOpenChildTask { get; set; }
        public int userId { get; set; }
        public int viewId { get; set; }
      
    }
}
