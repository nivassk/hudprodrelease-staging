﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("Prod_SubFolderStructure")]
    public partial class Prod_SubFolderStructure
    {
        [Key]
        public int FolderKey { get; set; }
        public string FolderName { get; set; }
        public int? ParentKey { get; set; }
        public int ViewTypeId { get; set; }
        public int FolderSortingNumber { get; set; }
        public string SubfolderSequence { get; set; }
        public string FhaNo { get; set; }
        public string ProjectNo { get; set; }
    }
}


