﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityObject.Entities.HCP_task
{
    [Table("ReviewFileComment")]
    public class ReviewFileComment
    {
        [Key]
        public Guid ReviewFileCommentId { get; set;}
        public Guid FileTaskId { get; set;}
        public string Comment { get; set;}
        public DateTime CreatedOn { get; set;}
        public int CreatedBy { get; set;}
        public int? ReviewerProdViewId { get; set; }
    }
}
