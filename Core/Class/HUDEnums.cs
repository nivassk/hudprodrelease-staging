﻿using System.ComponentModel;

namespace HUDHealthcarePortal.Core
{
    public enum DBSource
    {
        [Description("Null database")]
        None = 0,
        [Description("Staging database to store multiple uploads")]
        Intermediate = 1,
        [Description("Store quarterly data")]
        Live = 2,
        [Description("Task/Activity Workflow database")]
        Task = 3,
    }

    public enum HUDManagerType
    {
        NotSpecified = 0,
        WorkloadManager = 1,
        AccountExecutive = 2,
        InternalSpecialOptionUser = 3
    }

    public enum TaskStep
    {
        [Description("Not Specified"), Category("NotSpecified")]
        NotSpecified = 0,
        [Description("Final Step"), Category("Final")]
        Final = 1,
        [Description("Upload Form"), Category("FormUpload")]
        UploadForm = 2,
        [Description("Submit Form"), Category("FormUpload")]
        SubmitForm = 3,
        [Description("Non Critical Repair – In-Process"), Category("FormUpload")]
        NonCriticalRequest = 4,
        [Description("Reserve For Replacement In-Process"), Category("FormUpload")]
        R4RRequest = 5,
        [Description("Reserve For Replacement Complete"), Category("FormUpload")]
        R4RComplete = 6,
        [Description("Non Critical Repair - Complete"), Category("FormUpload")]
        NonCriticalRepairComplete = 7,
        [Description("Non Critical Request Extension In-Process"), Category("FormUpload")]
        NonCriticalRequestExtension = 12,
        [Description("Non Critical Request Extension Completed"), Category("FormUpload")]
        NonCriticalRequestExtensionComplete = 13,
        [Description("Project Action Request In-Process"), Category("FormUpload")]
        ProjectActionRequest = 14,
        [Description("Project Action Request - Complete"), Category("FormUpload")]
        ProjectActionRequestComplete = 15,
        [Description("Project Action Request - Addtional Information"), Category("FormUpload")]
        OPAAddtionalInformation = 16,
        [Description("Under Review"), Category("Reviewer")]
        UnderReview = 17,
        [Description("In Process")]
        InProcess = 18,
        [Description("Complete")]
        Complete = 19,
        [Description("FC Request")]
        Request = 20,
        [Description("FC Response")]
        Response = 21,
        [Description("Form290 In Process"), Category("FormUpload")]
        Form290Request = 22,
        [Description("Form290 Complete"), Category("FormUpload")]
        Form290Complete = 23,
    }

    public enum FormUploadStatus
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Assigned task to other")]
        AssignedToOther = 1,
        [Description("Assgined task by other")]
        AssignedByOther = 2,
        [Description("Done")]
        Done = 3,
    }

    public enum FinancialStatementType
    {
        [Description("(None)")]
        NotSpecified = 0,
        [Description("Audit")]
        Audit = 1,
        [Description("Certified")]
        Certified = 2,
        [Description("Other")]
        Other = 3
    }

    public enum OperatorOwner
    {
        [Description("(None)")]
        NotSpecified = 0,
        [Description("Operator")]
        Operator = 1,
        [Description("Owner-Operator")]
        OperatorOwner = 2,
    }

    public enum MonthsInPeriod
    {
        [Description("0")]
        NotSpecified = 0,
        [Description("1")]
        One = 1,
        [Description("2")]
        Two = 2,
        [Description("3")]
        Three = 3,
        [Description("4")]
        Four = 4,
        [Description("5")]
        Five = 5,
        [Description("6")]
        Six = 6,
        [Description("7")]
        Seven = 7,
        [Description("8")]
        Eight = 8,
        [Description("9")]
        Nine = 9,
        [Description("10")]
        Ten = 10,
        [Description("11")]
        Eleven = 11,
        [Description("12")]
        Twelve = 12,
    }

    public enum ReportYear
    {
        [Description("2014")]
        Year2014 = 0,
        [Description("2015")]
        Year2015 = 1
    }

    public enum HUDTimeZone
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Alaskan Standard Time")]
        Alaskan = 1,
        [Description("Central Standard Time")]
        Central = 2,
        [Description("Eastern Standard Time")]
        Eastern = 3,
        [Description("Hawaiian Standard Time")]
        Hawaiian = 4,
        [Description("Mountain Standard Time")]
        Mountain = 5,
        [Description("Pacific Standard Time")]
        Pacific = 6,
    }

    public enum HUDTimeZoneName
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("AKST")]
        Alaskan = 1,
        [Description("CST")]
        Central = 2,
        [Description("EST")]
        Eastern = 3,
        [Description("HST")]
        Hawaiian = 4,
        [Description("MST")]
        Mountain = 5,
        [Description("PST")]
        Pacific = 6,
    }

    public enum HUDRoleSource
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Internal User")]
        InternalUser = 1,
        [Description("External User")]
        ExternalUser = 2,
        [Description("Super User")]
        SuperUser = 3,
    }

    public enum HUDRole
    {
        [Description("Not Specified"), Category("NotSpecified")]
        NotSpecified = 0,
        [Description("Super User"), Category("SuperUser")]
        SuperUser = 1,
        [Description("HUD Admin"), Category("InternalUser")]
        HUDAdmin = 2,
        [Description("HUD Director"), Category("InternalUser")]
        HUDDirector = 3,
        [Description("Account Executive"), Category("InternalUser")]
        AccountExecutive = 4,
        [Description("Workload Manager"), Category("InternalUser")]
        WorkflowManager = 5,
        [Description("Attorney"), Category("InternalUser")]
        Attorney = 6,
        [Description("Lender Account Manager"), Category("ExternalUser")]
        LenderAccountManager = 7,
        [Description("Backup Account Manager"), Category("ExternalUser")]
        BackupAccountManager = 8,
        [Description("Special Option User"), Category("ExternalUser")]
        Servicer = 9,
        [Description("Lender Account Representative"), Category("ExternalUser")]
        LenderAccountRepresentative = 10,
        [Description("Operator Account Representative"), Category("ExternalUser")]
        OperatorAccountRepresentative = 11,
        [Description("Internal Special Option User"), Category("InternalUser")]
        InternalSpecialOptionUser = 12,

        [Description("Production User"), Category("InternalUser")]
        ProductionUser = 13,
        [Description("Reviewer"), Category("InternalUser")]
        Reviewer = 14,
        [Description("Production WLM"), Category("InternalUser")]
        ProductionWlm = 15,

        [Description("Inspection Contractor"), Category("ExternalUser")]
        InspectionContractor = 16
    }

    public enum DerivedFinancial
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Debt Service Coverage Ratio")]
        DebtCoverageRatio = 1,
        [Description("Working Capital")]
        WorkingCapital = 2,
        [Description("Days Cash on Hand")]
        DaysCashOnHand = 3,
        [Description("Days in Accounts Receivable")]
        DaysInAccountReceivable = 4,
        [Description("Average Payment Period")]
        AvgPaymentPeriod = 5,
        [Description("Reserve for Replacement Balance per unit")]
        ReserveForReplaceBalUnit = 6,
        [Description("Debt Service Coverage 2 Ratio")]
        DebtCoverageRatio2 = 7,
        [Description("Average Daily Rate")]
        AverageDailyRate = 8,
        [Description("NOI")]
        NOI = 9
    }

    public enum FinancialQuarter
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("03/31")]
        Q1 = 1,
        [Description("06/30")]
        Q2 = 2,
        [Description("09/30")]
        Q3 = 3,
        [Description("12/31")]
        Q4 = 4,
    }

    public enum ReportingPeriod
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Q1 {0}")]
        Q1 = 3,
        [Description("Q2 {0}")]
        Q2 = 6,
        [Description("Q3 {0}")]
        Q3 = 9,
        [Description("Q4 {0}")]
        Q4 = 12,
    }

    public enum ReportType
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Missing Project Report")]
        MissingProjectReport = 1,
        [Description("High Risk Report")]
        HighRiskProjectReport = 2,
        [Description("Workload Mgmt Report")]
        ProjectReport = 3,
        [Description("Large Loan Report")]
        LargeLoanReport = 4,
        [Description("Ratio Exceptions Report")]
        RatioExceptionsReport = 5,
        [Description("Project Action Management Report")]
        PAMReport = 6,
        [Description("Lender Project Action Management Report")]// User Story 1904
        LenderPAMReport = 17,
        [Description("Project Detail Report")]
        ProjectDetailReport = 7,

        [Description("Project Detail Report")]
        LenderProjectDetailReport = 14,

        [Description("Exception Report")]
        ProjectErrorReportAdmin = 8,

        [Description("Exception Report")]
        ProjectErrorReportWLM = 9,

        [Description("Exception Report")]
        ProjectErrorReportAE = 10,


        [Description("Point and Time  Report")]
        ProjectPTReport = 11,
        [Description("Point and Time  Report")]
        ProjectPTReportAE = 12,
        [Description("Point and Time Report")]
        ProjectPTReportWLM = 13,

        [Description("AE Current Quarter  Report")]
        AECurrentQuarterReport = 14,
        [Description("AE Current Quarter  Report")]
        WLMCurrentQuarterReport = 15,
        [Description("AE Current Quarter  Report")]
        AdminCurrentQuarterReport = 16,

        [Description("Production Project Action Management Report")]
        HudProductionReport = 18,
        [Description("Task Reassignment")]
        ProdTaskReassignment = 19,
        [Description("Production Lender Project Action Management Report")]
        ProductionLenderPAMReport = 20

    }

    public enum ReportLevel
    {
        [Description("Super User Level")]
        SuperUserLevel = 0,
        [Description("WLM Level")]
        WLMUserLevel = 1,
        [Description("AE Level")]
        AEUserLevel = 2,
        [Description("Lender Detail Level")]
        LenderDetailLevel = 3,
        // User Story 1904
        [Description("LAM Level")]
        LAMUserLevel = 4,
        [Description("BAM Level")]
        BAMUserLevel = 5,
        [Description("LAR Level")]
        LARUserLevel = 6,
        [Description("Internal AE Level")]
        IAEUserLevel = 7,
        [Description("ISOU Level")]
        ISOUUserLevel = 8,
        [Description("ProductionWLM Level")]
        ProductionWLMLevel = 9,
        [Description("ProductionUser Level")]
        ProductionUserLevel = 10
    }

    public enum ProjectReportVisibility
    {
        [Description("All Projects Assigned")]
        ProjectsAssigned = 0,
        [Description("Highlight High Risk")]
        HighRisk = 1
    }

    public enum ProjectReportGridColumnHeadings
    {
        [Description("Unpaid Principal Balance")]
        UPB = 0,
        [Description("High Loan")]
        HL = 1,
        [Description("Debt-Service Coverage Ratio")]
        DSCR = 2,
        [Description("Days Cash on Hand")]
        DCoH = 3,
        [Description("Average Payment Period")]
        APP = 4,
        [Description("Working Capital")]
        WC = 5,
        [Description("Accounts Receivable")]
        AR = 6
    }

    public enum ProjectDetailReportGridColumnHeadings
    {
        [Description("Cumulative Rev - Cumulative Exp")]
        NOICumulative = 0

    }

    public enum UploadStatusFlag
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Not Late")]
        NotLate = 1,
        [Description("Late")]
        Late = 2,
        [Description("Bad Data")]
        BadData = 3
    }

    public enum RatioExceptionsReportFilter
    {
        [Description("Debt Coverage Ratio (< 1.2)")]
        DebtCoverageRatio = 0,
        [Description("Working Capital (< 1.2)")]
        WorkingCapital = 1,
        [Description("Days Cash on Hand (< 14 days)")]
        DaysCashOnHand = 2,
        [Description("Days in Accounts Receivable (> 70 days)")]
        DaysInAccountsReceivable = 3,
        [Description("Average Payment Period (> 70 days)")]
        AveragePaymentPeriod = 4
    }

    public enum FileType
    {

        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Approval Advance 92464")]
        ApprovalAdvance92464 = 1,
        [Description("Default Request Extension")]
        DefaultRequestExtension = 2,
        [Description("Borrower Certification 92117")]
        BorrowerCertification92117 = 3,
        [Description("Final Inspection Report")]
        ClearTitle = 4,
        [Description("Scope Certification")]
        ScopeCertification = 5,
        [Description("9250")]
        File9250 = 6,
        [Description("9250A")]
        File9250A = 7,
        [Description("Invoice")]
        Invoice = 8,
        [Description("Receipt")]
        Receipt = 9,
        [Description("Contract")]
        Contract = 10,
        [Description("Picture")]
        Picture = 11,
        [Description("Other")]
        Other = 13,
        [Description("Servicer Input Form")]
        ServicerInput = 12,
        [Description("NCR Contract File")]
        FileContract = 14,
        [Description("Work Product")]
        WP = 15,
        [Description("Final work Product")]
        FWP = 16,
        [Description("Application Details")]
        appDetails = 17,
        [Description("General Info")]
        genInfo = 18,
        [Description("Miscellaneous Info")]
        miscInfo = 19,
        [Description("OGC Info")]
        ogcInfo = 20,
        [Description("Appraisal Info")]
        appraisalInfo = 21,
        [Description("Environmental Info")]
        envInfo = 22,
        [Description("Survey Info")]
        surveyInfo = 23,
        [Description("Contractor Info")]
        contractorInfo = 24,
        [Description("Closing Info")]
        closingInfo = 25,
        [Description("SharePointPdf")]
        SharePointPdf = 26,
        [Description("Lender Grid")]
        Lender = 27,
        [Description("Final Documents Grid")]
        FinalDoc = 28,
        [Description("PCNA")]
        PCNA = 29,
        [Description("Form290")]
        Form290 = 31,
		[Description("AmendmentsTemplate")]
		AmendmentsTemplate = 32,
		[Description("Reserve for Replacement Submission Form")]
		R4RSubmissionForm = 33,
	}

    public enum RequestStatus
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Submit")]
        Submit = 1,
        [Description("Approve")]
        Approve = 2,
        [Description("Approve with changes")]
        ApproveWithChanges = 3,
        [Description("Auto Approve")]
        AutoApprove = 4,
        [Description("DisApprove")]
        Deny = 5,
        [Description("Draft")]
        Draft = 6,
        [Description("Additional Information")]
        RequestAdditionalInfo = 7,
        [Description("Cancel")]
        Cancel = 8
    }


    public enum FormType
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Non Critical")]
        Ncr = 1,
        [Description("Reserve for Replacement")]
        R4R = 2
    }

    public enum TransactionType
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Non Critical Approval")]
        NCRReimbursement = 1
    }

    public enum RequestExtensionStatus
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Submit")]
        Submit = 1,
        [Description("Approve")]
        Approve = 2,
        [Description("Deny")]
        Deny = 3,
        [Description("Auto Approve")]
        AutoApprove = 4
    }
    public enum ProjectActionName
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Reserve for Replacement")]
        R4R = 1,
        [Description("Non-Critical")]
        NCR = 2,
        [Description("Non-Critical Extension")]
        NCRExtension = 3
    }

    public enum NCRLateTimeSpan
    {
        [Description("90 days")]
        Late90days = 0,
        [Description("180 days")]
        Late180days = 1,
        [Description("270 days")]
        Late270days = 2,
        [Description("Balance over 50% after 270 days")]
        Over50On270days = 3
    }

    public enum EmailType
    {
        [Description("Requests and inquiries")]
        RQST = 1,
        [Description("Recover password")]
        PWDRC = 2,
        [Description("Other")]
        OTHR = 3,
        [Description("Register new user")]
        REGISTER = 4,
        [Description("Comment for properties")]
        COMMENT = 5,
        [Description("Upload notification to AE")]
        UPLDNOTIFY = 6,
        [Description("R4R Auto approval notification")]
        AUTOAPPR = 7,
        [Description("R4R Submit Notification")]
        R4RSUBMIT = 8,
        [Description("R4R Approval notification")]
        R4RAPPR = 9,
        [Description("Missing FHA Number notification")]
        MISNGFHA = 10,
        [Description("NonCritical Email")]
        NonCrAPPR = 11,
        [Description("Email Notification to update iREMS")]
        UPDTIREMS = 12,
        [Description("Tier 1 Email Notification to submit Non-Critical Repair Request form")]
        NCRENA1 = 13,
        [Description("Tier 2 Email Notification to submit Non-Critical Repair Request form")]
        NCRENA2 = 14,
        [Description(" Project Action submission request")]
        ProjectActionSubmission = 17,
        [Description(" Project Action submission response from AE")]
        PARESPONSE = 18,
        [Description(" Task Reassignment Notification to AEs")]
        TSKREASSGN = 19,
        [Description("Production FHA Request Completion Notification")]
        FHARQST = 20,
        [Description("Production FHA# Insert Notification")]
        FHAINSERT = 21,
        [Description("Production Application Request Assignment Notification")]
        APPRQSTASSIGN = 23,
        [Description("Production Application Request Lender Submission Notification")]
        APPPRSUB = 22,
        [Description("Production Application Request Assignment Notification")]
        APPPRLCSUB = 24,
        [Description("Firm Commitment Issue Notification")]
        FRMCMMTMNT = 25,
        [Description("Early Start Approval Letter Notification")]
        ERSTRAPP = 26,
        [Description("Initial Closing Date schedule Notification")]
        INCLDATE = 27,
        [Description("IRR Initiate Request Notification")]
        IRRINIT = 28,
        [Description("Firm Commitment Approval notification")]
        FIRMAP = 29,
        [Description("Single Stage Submission Notification")]
        SSTGS = 30,
        [Description("Initial Submissions Notification")]
        INISUB = 31,
        [Description("Final Submissions Notification")]
        FNLS = 32,
        [Description("IRR Approval Notification")]
        IRRAP = 33,
        [Description("IRR Final Approval Notification")]
        IRRCLSG = 34,
        [Description("IRR Loan Modification Approval Recommendation")]
        IRRLMAR = 35,
        [Description("R4R Deny notification")]
        R4RDeny = 36,
        [Description("FORM 290 Closing                                  ")]
        FRM290CLOS = 37,

        [Description("NON CONSTRUCTION EXECUTED CLOSING")]
        NONCEC = 38,
        [Description("NON CONSTRUCTION DRAFT CLOSING ASSIGNED FROM QUE")]
        DCFROMQ = 39,
        [Description("NON CONSTRUCTION DRAFT CLOSING REJECTED")]
        DCREJECT = 40,
        [Description(" Task Reassignment Notification ")]
        PTSKRASSGN = 41,
		[Description("Amendments Request Denied")]
		DNYAMEND = 42,
		[Description("Amendment Review and Authorization")]
		WLMAMEND = 43,
        [Description("Firm Commitment Amendment")]
        WLMAMEND1 = 44,
		[Description("Amendment Email to Closer")]
		WLMAMEND2 = 45,
        [Description("Email Opa TPA Light")]
        TPALIGHT = 46,
        [Description("Lender Submit Amendment Request")]
        LENSUBAMND = 47,

        [Description("Firm Commitment Approval notification with out OGC")]
        FIRMAP2 = 50,
        [Description("Amendment Email to UWCLOSER")]
        AMUWCLOSER = 49,
    }

    public enum StatusForProjectActionAEForm
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Approved")]
        A = 1,
        [Description("Request Additional Information")]
        R = 2
    }

    public enum ReviewerStatus
    {

        [Description("Under Review")]
        UnderReview = 1,
        [Description("Review Completed")]
        ReviewCompleted = 2

    }

    //public enum FhaNumberRequestTypes
    //{   
    //    [Description("FHA # Insert")]
    //    InsertFha =8,
    //    [Description("Portfolio")]
    //    Portfolio = 9,
    //    [Description("Credit Review")]
    //    CreditReview =10,
    //    [Description("Under Writer")]
    //    UW = 1,
    //    [Description("Survey Title")]
    //    ST = 2,
    //    [Description("Environmentalist")]
    //    ENV = 3,

    //}

    public enum PageType
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("FHA Request")]
        FhaRequest = 4,
        [Description("Production Application")]
        ProductionApplication = 5,
        [Description("ConstructionSingleStage")]
        ConstructionSingleStage = 6,
        [Description("ConstructionTwoStageInitial")]
        ConstructionTwoStageInitial = 7,
        [Description("ConstructionTwoStageFinal")]
        ConstructionTwoStageFinal = 8,
        [Description("ConstructionManagement")]
        ConstructionManagement = 9,
        //[Description("ClosingAllExceptConstruction")]
        [Description("Draft Closing")]
        ClosingAllExceptConstruction = 10,
        [Description("Executed Closing")]
        ExecutedClosing = 17,
        [Description("Initial Closing")]
        ClosingConstructionInsuredAdvances = 11,
        [Description("Final Closing")]
        ClosingConstructionInsuranceUponCompletion = 12,
        //Internal External Task types
        [Description("NOI")]
        NOI = 13,
        [Description("REAC")]
        REAC = 14,
        [Description("OTHER")]
        OTHER = 15,
        [Description("Form 290")]
        Form290=16,
        [Description("Amendments")]
        Amendments = 18,

    }

    public enum AdditionalFinancingResources
    {
        [Description("LIHTC")]
        LIHTC = 1,
        [Description("Tax-Exempt Bonds")]
        TAX = 2,
        [Description("HOME")]
        HOME = 3,
        [Description("CDBG")]
        CDBG = 4,
        [Description("Other")]
        OTHER = 5
    }
    public enum ProductionFhaRequestStatus
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("Review Completed")]
        ReviewCompleted = 15,
        [Description("In Queue")]
        InQueue = 17,
        [Description("In Proccess")]
        InProcess = 18,
        [Description("FHA Request Completed")]
        Completed = 19

    }

    public enum ProductionAppProcessStatus
    {
        [Description("Not Specified")]
        NotSpecified = 0,
        [Description("In Queue")]
        InQueue = 17,
        [Description("In Process")]
        InProcess = 18,
        [Description("Application Request Completed")]
        Completed = 19,
        [Description("Firm Commitment Request")]
        Request = 20,
        [Description("Firm Commitment Response")]
        Response = 21,
        //TODO:this enum value must be removed 
        [Description("Application Complete")]
        ProjectActionRequestComplete = 15,
        [Description("Form290 In Process")]
        Form290InQueue = 22,
        [Description("Form290 Request Completed")]
        Form290Complete = 23,
    }

    #region Production Application

    public enum ProductionView
    {
        [Description("UnderWriter")]
        UnderWriter = 1,
        [Description("Survey")]
        Survey = 2,
        [Description("Environmentalist")]
        Environmentalist = 3,
        [Description("Attorney")]
        Attorney = 4,
        [Description("DEC")]
        DEC = 5,
        [Description("Contract UW")]
        ContractUW = 6,
        [Description("Contract Closer")]
        ContractCloser = 7,
        [Description("FHA # Insert")]
        InsertFha = 8,
        [Description("Portfolio")]
        Portfolio = 9,
        [Description("Credit Review")]
        CreditReview = 10,
        [Description("Backup OGC")]
        BackupOGC = 11,
        [Description("Appraiser")]
        Appraiser = 12,
		[Description("WLM")]
		WLM = 13,
		[Description("DAP")]
		DAP = 14,
	}


    public enum ReviewFileStatusList
    {
        [Description("NewUnread")]
        NewUnread = 1,
        [Description("RRUnread ")]
        RRUnread = 2,
        [Description("Read")]
        Read = 3,
        [Description("RAI")]
        RAI = 4,
        [Description("Approved")]
        Approved = 5,
        [Description("RAIReq")]
        RAIReq = 6,
    }
    #endregion

    public enum ApplicationAndClosingType
    {
        [Description("Application")]
        application = 5,
        [Description("Single Stage Application")]
        ConstructionSingleStage = 6,
        [Description("Initial Application")]
        ConstructionTwoStageInitial = 7,
        [Description("Final Application")]
        ConstructionTwoStageFinal = 8,
        //[Description("Initial Closing")]
        //closingInital=9,
        [Description("Non-Construction Draft Closing")]
        Draftclosing = 10,
        [Description("Non-Construction Executed Closing")]
        Executedclosing = 17,
        [Description("Final Closing")]
        closingFinal = 11,
        [Description("Initial Closing")]
        closingInital = 11,
        [Description("Final Closing")]
        closingTwoStageFinal = 12,
		[Description("Amendments")]
		Amendments = 18

	}

    public enum SharepointSections
    {
        [Description("All")]
        all = 0,
        [Description("Application Details")]
        appDetails = 1,
        [Description("General Info")]
        genInfo = 2,
        [Description("Miscellaneous Info")]
        miscInfo = 3,
        [Description("OGC Info")]
        ogcInfo = 4,
        [Description("Appraisal Info")]
        appraisalInfo = 5,
        [Description("Environmental Info")]
        envInfo = 6,
        [Description("Survey Info")]
        surveyInfo = 7,
        [Description("Contractor Info")]
        contractorInfo = 8,
        [Description("Closing Info")]
        closingInfo = 9,
        [Description("Ammendment Info")]
        ammendmentInfo = 10,
        [Description("Loan Committee Info")]
        lcInfo = 11
    }

    public enum ProdProjectTypes
    {
        [Description("Purchase/Refinance 223(f)")]
        Purchase = 1,
        [Description("Refinance 223(a)(7)")]
        Refinance = 2,
        [Description("OLL 223(d)")]
        OLL = 3,
        [Description("Fire Safety 223(i)")]
        FireSafety = 4,
        [Description("Construction 241(a)")]
        Construction241 = 5,
        [Description("Construction NC")]
        ConstructionNC = 6,
        [Description("Construction SR")]
        ConstructionSR = 7,
        [Description("IRR")]
        IRR = 8
    }
    public enum ProdFolderStructure
    {

        [Description("Accounts Receivable Financing Documents")]
        AccountsReceivableFinancing = 12,
        [Description("Additional Funding Sources")]
        AdditionalFundingSources = 24,
        [Description("Asset Management Controlling Documents")]
        AssetManagementControlling = 3,
        [Description("Borrower")]
        Borrower = 4,
        [Description("Closing")]
        Closing = 5,
        [Description("Work Product Closing")]
        WorkProductClosing = 29,
        [Description("Construction and Architectural Documents")]
        ConstructionAndArchitectural = 6,
        [Description("Contractor")]
        Contractor = 7,
        [Description("Work Product Loan Committee Folder")]
        WorkProductLoanCommittee = 8,
        [Description("Management Agent")]
        ManagementAgent = 9,
        [Description("Master Lease Documents")]
        MasterLeaseDocuments = 10,
        [Description("Master Tenant")]
        MasterTenant = 11,
        [Description("New file request comment by HUD")]
        NewFileRequestComment = 1,
        [Description("Non-Critical Repair")]
        NonCriticalRepair = 13,
        [Description("Operations")]
        Operations = 14,
        [Description("Operator A")]
        OperatorA = 15,
        [Description("Operator B")]
        OperatorB = 16,
        [Description("Parent of Operator")]
        ParentOfOperator = 17,
        [Description("Principal of Borrower A")]
        PrincipalOfBorrowerA = 18,
        [Description("Principal of Borrower B")]
        PrincipalOfBorrowerB = 19,
        [Description("Principal of Borrower C")]
        PrincipalOfBorrowerC = 20,
        [Description("Professional Liability Insurance (PLI)")]
        PLI = 21,
        [Description("R4R")]
        R4R = 22,
        [Description("Real Estate")]
        RealEstate = 23,
        [Description("Submit(s)")]
        Submits = 2,
        [Description("Third-Party Reports")]
        ThirdPartyReports = 25,
        [Description("TPA")]
        TPA = 26,
        [Description("Underwriting")]
        Underwriting = 27,
        [Description("Work Product Underwriting")]
        WorkProductUnderwriting = 28,
        [Description("Form290")]
        Form290 = 31,
		[Description("Amendment Template")]
		AmendmentTemplate =38
	}
    public enum InternalExternalTaskStatus
    {
        [Description("Draft")]
        Draft = 1,
        [Description("Approved")]
        Approved = 2,
        [Description("Submitted")]
        Submitted = 3,
        [Description("Completed")]
        completed = 4
    }

	public enum Prod_FolderStructureApplicationFolderKey
	{
		[Description("Section00")]
		Section00 = 3,
		[Description("Section3")]
		Section3 = 4,
		[Description("Section17")]
		Section17 = 5,
		[Description("Section15")]
		Section15 = 6,
		[Description("Section14")]
		Section14 = 7,
		[Description("Section7")]
		Section7 = 9,
		[Description("Section13")]
		Section13 = 10,
		[Description("Section18")]
		Section18 = 11,
		[Description("Section12")]
		Section12 = 12,
		[Description("Section9")]
		Section9 = 14,
		[Description("Section5")]
		Section5 = 15,
		[Description("Section6")]
		Section6 = 17,
		[Description("Section4")]
		Section4 = 18,
		[Description("Section10")]
		Section10 = 21,
		[Description("Section8")]
		Section8 = 23,
		[Description("Section11")]
		Section11 = 24,
		[Description("Section2")]
		Section2 = 25,
		[Description("Section1")]
		Section1 = 27
	}

	public enum Prod_FolderStructureNCFolderKey
	{
		[Description("Section00")]
		Section00 = 3,
		[Description("Section3")]
		Section3 = 4,
		[Description("Section17")]
		Section15 = 6,
		[Description("Section14")]
		Section14 = 7,
		[Description("Section7")]
		Section7 = 9,
		[Description("Section13")]
		Section13 = 10,
		[Description("Section18")]
		Section18 = 11,
		[Description("Section12")]
		Section12 = 12,
		[Description("Section9")]
		Section9 = 14,
		[Description("Section5")]
		Section5 = 15,
		[Description("Section6")]
		Section6 = 17,
		[Description("Section4")]
		Section4 = 18,
		[Description("Section10")]
		Section10 = 21,
		[Description("Section8")]
		Section8 = 23,
		[Description("Section11")]
		Section11 = 24,
		[Description("Section2")]
		Section2 = 25,
		[Description("Section1")]
		Section1 = 27,
		[Description("Section17")]
		Section17 = 30
	}

    public enum LoanCommitteeDecision
    {        
        [Description("Reject")]
        Reject = 0,
        [Description("Approve")]
        Approve = 1,
        [Description("Withdrawn")]
        Withdrawn = 2,
        [Description("Request Additional Information")]
        RequestAdditionalInformation = 3
    }

	public enum SharepointSectionFromMyTasks
	{
		[Description("All")]
		all = 0,
		[Description("Application")]
		Application = 4,
		[Description("Amendment")]
		Amendment = 14,

	}

	public enum AmendmentTypeSP
	{
		[Description("Party or Property Name")]
		Party_Propert_Name_cb = 1,
		[Description("Party or Property Address")]
		Party_Property_Address_Name_cb = 2,
		[Description("Loan Amount")]
		LoanAmount_cb = 3,
		[Description("Interest Rate")]
		InterestRate_cb = 4,
		[Description("Without change in Monthly P&I Payment")]
		MonthlyPayment_cb = 5,
		[Description("With change in Monthly P&I Payment")]
		DifferentMonthlyPayment_cb = 6,
		[Description("Extension")]
		CommitmentTerminationDate_cb = 7,
		[Description("Initial or Additional Reserves Deposit")]
		R4RAmount_cb = 8,
		[Description("Critical Repairs")]
		CriticalRepairCost_cb = 9,
		[Description("Non-Critical and Borrower Elective Repairs")]
		RemainingRepairCostExihibitC_cb = 10,
		[Description("Special Condition")]
		SpecialCondition_cb = 11,
		[Description("Operating Lease Payment")]
		AnnualLeasePayment_cb = 12,
		[Description("Other")]
		Other_cb = 13,
		[Description("Exhibit Replacement")]
		ExhibitLetter_cb = 14,
	}

	public enum PreviousAmendmentStage
	{
		[Description("Amendment Draft Closing")]
		DraftExist = 1,
		[Description("Amendment Application")]
		ApplicationExist = 2,
		[Description("Amendment Executed")]
		ExecutedExist = 3,
	}
}
