﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetTasksByUserName]
(
	@UserName NVARCHAR(150)
)
AS
DECLARE @IsUserInternal varchar(100)
DECLARE @HudToken VARCHAR(8)
DECLARE @IsHudUser BIT
SET @HudToken = '@hud.gov'

IF Exists(
	SELECT * 
	FROM [$(LiveDB)].[dbo].[HCP_Authentication] auth
		JOIN [$(LiveDB)].[dbo].[webpages_UsersInRoles] uir ON uir.UserId = Auth.UserID 
		JOIN [$(LiveDB)].[dbo].[webpages_Roles] roles ON roles.RoleId = uir.RoleId 
	WHERE auth.UserName = @UserName and roles.RoleName in ('AccountExecutive','HUDAdmin', 'HUDDirector','WorkflowManager')
)
 BEGIN 
	 SET @IsUserInternal = 'true'
 END

IF LOWER(RIGHT(@UserName, 8)) = @HudToken
	SET @IsHudUser = 1
ELSE
	SET @IsHudUser = 0

IF(@IsUserInternal = 'true')
BEGIN
select [TaskId], [TaskInstanceId], [SequenceId], [AssignedBy], 
    [AssignedTo], [DueDate], [StartTime], [Notes], [TaskStepId], 
	[DataKey1], [DataKey2], [DataStore1], [DataStore2], 
	[TaskOpenStatus], PropertyName, FhaNumber,PageTypeId
from 
(
	SELECT T.[TaskId], T.[TaskInstanceId], T.[SequenceId], T.[AssignedBy]
		 ,T.[AssignedTo], T.[DueDate], T.[StartTime], T.[Notes], T.[TaskStepId]
		 ,T.[DataKey1], T.[DataKey2], T.[DataStore1], T.[DataStore2]
		 ,T.[TaskOpenStatus], O.PropertyName, T.FhaNumber,T.PageTypeId
		 ,ROW_NUMBER() OVER (PARTITION BY T.[TaskId] ORDER BY T.[TaskId] DESC) AS rn
	FROM 
	(
		SELECT TaskInstanceId, AssignedBy AS username
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId, AssignedBy
		UNION
		SELECT TaskInstanceId, AssignedTo AS username
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId, AssignedTo
	) a INNER JOIN [$(DatabaseName)].[dbo].[Task] T on a.TaskInstanceId = t.TaskInstanceId
		INNER JOIN 
	(
		SELECT MAX(SequenceId) AS maxSeqId, TaskInstanceId
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId
	) TMAX ON T.TaskInstanceId = TMAX.TaskInstanceId AND T.SequenceId = TMAX.maxSeqId
		INNER JOIN 
	(
		SELECT 
		   CASE 
			  WHEN @IsHudUser=1 THEN MAX(TaskId) ELSE MIN(TaskId) 
		   END AS MaxMinTaskId, 
		   TaskInstanceId
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId, SequenceId
	) TSeq ON T.TaskInstanceId = TSeq.TaskInstanceId AND T.TaskId = TSeq.MaxMinTaskId
	left join [$(LiveDB)].dbo.OPAForm O on T.FhaNumber = O.FhaNumber
	where a.username = @UserName and (IsReassigned is null OR IsReassigned = 0)
	--where t.TaskId > 19640
) OT where rn = 1
END

ELSE
BEGIN
select [TaskId], [TaskInstanceId], [SequenceId], [AssignedBy], 
    [AssignedTo], [DueDate], [StartTime], [Notes], [TaskStepId], 
	[DataKey1], [DataKey2], [DataStore1], [DataStore2], 
	[TaskOpenStatus], PropertyName, FhaNumber,PageTypeId
from 
(
	SELECT T.[TaskId], T.[TaskInstanceId], T.[SequenceId], T.[AssignedBy]
		 , T.[AssignedTo], T.[DueDate], T.[StartTime], T.[Notes], T.[TaskStepId]
		 , T.[DataKey1], T.[DataKey2], T.[DataStore1], T.[DataStore2]
		 , T.[TaskOpenStatus], O.PropertyName, T.FhaNumber,T.PageTypeId,
		 ROW_NUMBER() OVER (PARTITION BY T.[TaskId] ORDER BY T.[TaskId] DESC) AS rn
	FROM 
	(
		SELECT TaskInstanceId, AssignedBy AS username
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId, AssignedBy
		UNION
		SELECT TaskInstanceId, AssignedTo AS username
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId, AssignedTo
	) a INNER JOIN  [$(DatabaseName)].[dbo].[Task] T on a.TaskInstanceId = t.TaskInstanceId
	INNER JOIN 
	(
		SELECT MAX(SequenceId) AS maxSeqId, TaskInstanceId
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId
	) TMAX ON T.TaskInstanceId = TMAX.TaskInstanceId AND T.SequenceId = TMAX.maxSeqId
	INNER JOIN 
	(
		SELECT 
		   CASE WHEN @IsHudUser=1 THEN MAX(TaskId) ELSE MIN(TaskId) 
		   END AS MaxMinTaskId, 
		   TaskInstanceId
		FROM [$(DatabaseName)].[dbo].[Task]
		GROUP BY TaskInstanceId, SequenceId
	) TSeq ON T.TaskInstanceId = TSeq.TaskInstanceId AND T.TaskId = TSeq.MaxMinTaskId
	left join [$(LiveDB)].dbo.OPAForm O on T.FhaNumber = O.FhaNumber
	where a.username = @UserName 
) OT where rn = 1
END


--select * from PageTypeId

