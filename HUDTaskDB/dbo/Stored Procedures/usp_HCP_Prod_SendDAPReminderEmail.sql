﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_SendDAPReminderEmail]
As
Begin
	Declare @profile_name varchar(max)
	Declare @from_address varchar(max)
	Declare @recipients varchar(max)
	Declare @body_format varchar(10)
	Declare @importance varchar(10)
	Declare @subject varchar(max)
	Declare @body varchar(max)
	Declare @sharepointPage varchar(max)
	Declare @FHANumber varchar(max)
	Declare @ammendmentNumber varchar(max)
	Declare @assignedBy_email varchar(15)

	Set @profile_name = 'CloudMail' 
	Set @body_format = 'HTML' 
	Set @importance = 'HIGH'
	Set @sharepointPage = 'https://www.232hudhealthcare.com/'
	

	if OBJECT_ID('TEMPDB..#tmpDAPRem') is not null
    drop table #tmpDAPRem;

 
  select  
  'REMINDER: Update DAP and Sharepoint for Firm Commitment Amendment [' + CONVERT(varchar(10), amtask.FirmAmendmentNum) + '] ' + ' –' + ' ' + amtask.LenderCompanyName  + ' , ' + amtask.FHANumber  as mail_subject,
  '<p>As a reminder, [a] Firm Commitment Amendment' + ' <b> [' + CONVERT(varchar(10), amtask.FirmAmendmentNum) + '] </b>'+ ' was issued back on' +  ' ' + CONVERT(nvarchar(30),t.ModifiedOn) + '. Please update DAP and ' + '<a href=' + 'https://232hudhealthcare.com' + '>the SharePoint page in the Portal</a>' + ' as soon as possible.<p>'  as mail_body,
  'HHcPSupport@testing232healthcare.com' as from_Address,
   au.username as recipients, --'rchitturi@c3-systems.com' as recipients, 
   au1.UserID as assignedBy_email
  into #tmpDAPRem
  from [$(DatabaseName)].dbo.Prod_FormAmendmentTask  amtask
  join Prod_TaskXref t on amtask.TaskInstanceId = t.TaskInstanceId
  join [$(DatabaseName)].dbo.Task task on amtask.TaskInstanceId = task.TaskInstanceId and t.TaskId = task.TaskId
  join [$(LiveDB)].dbo.HCP_Authentication au on au.UserID = t.AssignedTo
  join [$(LiveDB)].dbo.HCP_Authentication au1 on au1.UserID = t.AssignedBy
  where t.completedon is null and DATEDIFF(DAY, amtask.FirmCommitmentSignedDate, getdate()) > 2
 
DECLARE email_Cursor CURSOR FOR  
SELECT 
	 mail_Subject, 
	 mail_Body, 
	 from_Address ,
	 recipients,
	 assignedBy_email
	FROM #tmpDAPRem;  
OPEN email_Cursor;  
	FETCH NEXT FROM email_Cursor into @subject,@body,@from_Address , @recipients,@assignedBy_email;  
WHILE @@FETCH_STATUS = 0  
   BEGIN  
		
		  Exec msdb.dbo.sp_send_dbmail @profile_name = @profile_name, @from_address = @from_address,
								@recipients = @recipients, 
								@subject = @subject,@body = @body,@body_format = @body_format,
								@importance = @importance;

		-- Insert the values to the HCP email
		insert into [$(LiveDB)].dbo.HCP_Email values (@recipients,@from_Address,NULL,NULL,@subject,@body,@body,getdate() ,@assignedBy_email, NULL,NULL,NULL,48)

		FETCH NEXT FROM email_Cursor into @subject,@body,@from_Address , @recipients,@assignedBy_email;   
   END;  
CLOSE email_Cursor;  

DEALLOCATE email_Cursor; 

drop table #tmpDAPRem;
 																	

	

  
End


--exec [dbo].[usp_HCP_Prod_SendDAPReminderEmail]
