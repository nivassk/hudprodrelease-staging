﻿
CREATE TABLE [dbo].[TaskConcurrency](
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[Concurrency] [timestamp] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
 CONSTRAINT [PK_TaskConcurrency] PRIMARY KEY CLUSTERED 
(
	[TaskInstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

