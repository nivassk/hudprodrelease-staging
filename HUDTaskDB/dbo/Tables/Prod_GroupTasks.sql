﻿
CREATE TABLE [dbo].[Prod_GroupTasks](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[RequestStatus] [int] NOT NULL,
	[InUse] [int] NULL,
	[PageTypeId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[IsDisclaimerAccepted] [bit] NULL,
	[ServicerComments] [varchar](max) NULL,
 CONSTRAINT [PK__Prod_Gro__7C6949B1C7361844] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]