﻿
CREATE TABLE [dbo].[Prod_SubFolderStructure](
	[FolderKey] [int] IDENTITY(1000,1) NOT NULL,
	[FolderName] [nvarchar](max) NOT NULL,
	[ParentKey] [int] NULL,
	[ViewTypeId] [int] NULL,
	[FolderSortingNumber] [int] NULL,
	[SubfolderSequence] [nchar](10) NULL,
	[FhaNo] [nchar](10) NULL,
	[ProjectNo] [nchar](10) NULL,
    [CreatedBy] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [PageTypeId] INT NULL, 
    [GroupTaskInstanceId] UNIQUEIDENTIFIER NULL, 
    CONSTRAINT [PK_Prod_SubFolderStructure] PRIMARY KEY CLUSTERED 
(
	[FolderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
