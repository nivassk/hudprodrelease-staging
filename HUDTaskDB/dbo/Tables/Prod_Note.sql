﻿
CREATE TABLE [dbo].[Prod_Note](
	[NoteId] [int] IDENTITY(1,1) NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[Note] [nvarchar](max) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Prod_Note] PRIMARY KEY CLUSTERED 
(
	[NoteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prod_Note]  WITH CHECK ADD  CONSTRAINT [FK_Prod_Note_Prod_Note] FOREIGN KEY([NoteId])
REFERENCES [dbo].[Prod_Note] ([NoteId])
GO

ALTER TABLE [dbo].[Prod_Note] CHECK CONSTRAINT [FK_Prod_Note_Prod_Note]
GO

