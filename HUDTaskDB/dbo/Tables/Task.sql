﻿
CREATE TABLE [dbo].[Task](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[SequenceId] [int] NOT NULL,
	[AssignedBy] [nvarchar](150) NOT NULL,
	[AssignedTo] [nvarchar](160) NULL,
	[DueDate] [datetime] NULL,
	[StartTime] [datetime] NOT NULL,
	[Notes] [nvarchar](1000) NULL,
	[TaskStepId] [int] NOT NULL,
	[DataKey1] [int] NULL,
	[DataKey2] [int] NULL,
	[DataStore1] [xml] NULL,
	[DataStore2] [xml] NULL,
	[TaskOpenStatus] [xml] NULL,
	[IsReassigned] [bit] NULL,
	[PageTypeId] [int] NULL,
	[FhaNumber] [varchar](15) NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Task]  WITH NOCHECK ADD  CONSTRAINT [FK_Task_TaskStep] FOREIGN KEY([TaskStepId])
REFERENCES [dbo].[TaskStep] ([TaskStepId])
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_TaskStep]
GO

