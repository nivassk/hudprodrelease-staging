﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
    public class UploadStatusChartModel
    {
        public int Received { get; internal set; }
        public int Expected { get; internal set; }
        public int Missing { get; internal set; }

        internal UploadStatusChartModel(IList<UploadStatusViewModel> uploadStatuses)
        {
            Received = Expected = Missing = 0;
            foreach(var item in uploadStatuses)
            {
                Expected += item.TotalExpected ?? 0;
                Missing += item.Missing ?? 0;
                Received += item.Received ?? 0;
            }
        }
    }
    public class UploadStatusModelUI
    {
        private IList<UploadStatusViewModel> _uploadStatuses;
        private UploadStatusChartModel _chartModel;
        public UploadStatusModelUI()
        {
            AllLenders = new List<SelectListItem>();
            AllQuarters = new List<SelectListItem>();
        }
        public IList<SelectListItem> AllLenders { get; set; }

        [Display(Name = "Select Lender")]
        public int SelectedLenderId { get; set; }

        public UploadStatusChartModel ChartModel
        {
            get { return _chartModel; }
        }
        public IList<UploadStatusViewModel> UploadStatuses 
        {
            get { return _uploadStatuses; }
            set
            {
                _uploadStatuses = value;
                _chartModel = new UploadStatusChartModel(_uploadStatuses);
            }
        }
        public IList<SelectListItem> AllQuarters { get; set; }
        public String SelectedQuarter { get; set; }
        public String ReportingPeriod { get; set; }
    }
}
