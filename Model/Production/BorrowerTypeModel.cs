﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class BorrowerTypeModel
    {
        public int BorrowerTypeId { get; set; }
        public string BorrowerTypeName { get; set; }
    }
}
