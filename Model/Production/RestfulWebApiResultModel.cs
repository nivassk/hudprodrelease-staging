﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
  public class RestfulWebApiResultModel
    {
        public string status { get; set; }
        public string key { get; set; }
        public string docId { get; set; }
        public string message { get; set; }
        public string version { get; set; }
        public string fileName { get; set; }
        public string folderName { get; set; }
        public string documentType { get; set; }
        public string indexType { get; set; }
        public string indexValue { get; set; }
        public object listData { get; set; }
        public object documents { get; set; }
    }

}
