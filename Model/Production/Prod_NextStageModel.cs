﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{

   
    public class Prod_NextStageModel
    {
        public Prod_NextStageModel()
        {

        }
        public Guid NextStgId { get; set; }
        public int CompletedPgTypeId { get; set; }
        public int NextPgTypeId { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public string FhaNumber { get; set; }
        public Guid? CompletedPgTaskInstanceId { get; set; }
        public Guid? NextPgTaskInstanceId { get; set; }
        public Boolean? IsNew { get; set; }

        
    }
}
