﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class FileReorderModel : Prod_TaskXrefModel
    {
        public int CurrentUserId { get; set; }
 
        public string FileIdlist { get; set; }

        public int AvailableFolders { get; set; }

        public int FromFolder { get; set; }
        
        public int pageTypeId { get; set; }

    }

    
}
