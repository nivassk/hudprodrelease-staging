﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HUDHealthcarePortal.Model
{
    public class AdditionalInformationModel
    {
        public Guid AdditionalInformationId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public string AEComment { get; set; }
        public string LenderComment { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public int? OrderNumber { get; set; }
        public Guid ParentFormID { get; set; }
    }
}
