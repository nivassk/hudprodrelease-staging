﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.AssetManagement
{
   public class RFORRANDNCR_GroupTaskModel
    {
        public int TaskId { get; set; }
        public Guid TaskInstanceId { get; set; }
        public int RequestStatus { get; set; }
        public int InUse { get; set; }
        public int PageTypeId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ServicerComments { get; set; }
        //GroupTask Grid Model
        public int GroupTaskid { get; set; }
        public string ProjectActionName { get; set; }
        public string Status { get; set; }
        public string role { get; set; }
        public string UnLock { get; set; }
        public DateTime? ServicerSubmissionDate { get; set; }
        public bool IsDisclaimerAccepted { get; set; }
        public string InUseuser { get; set; }
        public string FHANumber { get; set; }
    }
}
